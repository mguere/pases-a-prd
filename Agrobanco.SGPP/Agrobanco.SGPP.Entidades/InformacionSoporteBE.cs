﻿using System.Collections.Generic;

namespace Agrobanco.SGPP.Entidades
{
    public class InformacionSoporteBE
    {
        public string Titulo { get; set; }
        public List<Item> Item { get; set; }
    }

    public class Item
    {
        public string Descripcion { get; set; }
    }
    public class ObtenerInformacionSoporteBE
    {
        public string Titulo { get; set; }
        public string Item { get; set; }
    }
}
