﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SGPP.Entidades
{
    public class EstadosPaseBE
    {
        public int Id { get; set; }
        public string CodEstado { get; set; }
        public string Descripcion { get; set; }
      
    }
}
