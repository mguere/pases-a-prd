﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SGPP.Entidades
{
    public class TiposPlataformaBE
    {
        public int CodTipoPlataforma { get; set; }
        public string Descripcion { get; set; }
      
    }
}
