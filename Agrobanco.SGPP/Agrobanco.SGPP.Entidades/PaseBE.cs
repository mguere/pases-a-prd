﻿namespace Agrobanco.SGPP.Entidades
{
    public class PaseBE
    {
        public int CodPaseProduccion { get; set; }
        public int Analista { get; set; }
        public string Ticket { get; set; }
        public int TipoPase { get; set; }
        public string Descripcion { get; set; }
        public string UbicacionFuente { get; set; }
        public bool PaseInterno { get; set; }
        public int LibreriaFuente { get; set; }
        public int LibreriaObjetoOtros { get; set; }
        public int LibreriaObjetoPFyLF { get; set; }
        public int MotivoPase { get; set; }
        public int Estado { get; set; }
        public string Comentario { get; set; }
    }
}
