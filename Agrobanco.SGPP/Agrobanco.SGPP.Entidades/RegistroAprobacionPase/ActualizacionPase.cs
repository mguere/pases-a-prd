﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SGPP.Entidades.RegistroAprobacionPase
{
    public  class ActualizacionPase
    {
        public int CodPase { get; set; }
        public string Comentario { get; set; }
        public string UsuarioRegistro { get; set; }
        public string Perfil { get; set; }
    }

    public class RespuestaActualizacion
    {
        public bool Procesado { get; set; }
        public string Mensaje { get; set; }
    }
}
