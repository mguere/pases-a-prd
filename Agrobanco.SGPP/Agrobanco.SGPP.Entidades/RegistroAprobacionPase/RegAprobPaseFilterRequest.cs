﻿namespace Agrobanco.SGPP.Entidades.RegistroAprobacionPase
{
    public class RegAprobPaseFilterRequest
    {
        public string Perfil { get; set; }
        public int CodEstado { get; set; }
        public int CodTipoPlataforma { get; set; }       
        public string FechaRegistroIni { get; set; }
        public string FechaRegistroFin { get; set; }
       
    }
}
