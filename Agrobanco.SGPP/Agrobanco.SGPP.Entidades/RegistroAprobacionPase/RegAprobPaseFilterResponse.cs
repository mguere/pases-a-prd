﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SGPP.Entidades.RegistroAprobacionPase
{
    public class RegAprobPaseFilterResponse
    {       
        public int IDPase { get; set; }
        public string Nombre{ get; set; }
        public string Descripcion { get; set; }
        public string CreadoPor { get; set; }
        public string AprobadoPor { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public string Estado { get; set; }
        public string Pase { get; set; }      

    }
}
