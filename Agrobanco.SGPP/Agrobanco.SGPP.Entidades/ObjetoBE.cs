﻿namespace Agrobanco.SGPP.Entidades
{
    public class ObjetoBE
    {
        public int CodObjeto { get; set; }
        public int Tipo { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}
