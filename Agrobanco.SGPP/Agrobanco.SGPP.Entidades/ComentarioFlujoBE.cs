﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SGPP.Entidades
{
    public class ComentarioFlujoBE
    {
        public int CodPaseProduccion { get; set; }
        public string NombresApellidos { get; set; }
        public DateTime Fecha { get; set; }
        public string Comentario { get; set; }
    }
}
