﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SGPP.Entidades.RegistroPase
{
    public class DetallePaseRequest
    {
        public int CodPaseProduccion { get; set; }
        public string Ticket { get; set; }
        public int TipoPase { get; set; }
        public string TipoDescripcion { get; set; }
        public string FechaPase { get; set; }
        public string CodUsuario { get; set; }
        public string NombresApellidos { get; set; }
        public string Descripcion { get; set; }
        public string UbicacionFuente { get; set; }
        public int PaseInterno { get; set; }
        public int LibreriaFuente { get; set; }
        public int LibreriaObjetoOtros { get; set; }
        public int LibreriaObjetoPFyLF { get; set; }
        public int MotivoPase { get; set; }
        public int Estado { get; set; }
        public string EstadoDescripcion { get; set; }
        public string Comentario { get; set; }
        public List<ObjetoRequest> Objetos { get; set; }
        public List<ArchivoRequest> Archivos { get; set; }
        public List<SustentoRequest> Sustentos { get; set; }
        public List<FlujoReporteRequest> FlujoReporte { get; set; }
    }

    public class ObjetoRequest
    {
        public int CodObjeto { get; set; }
        public int Tipo { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }

    public class ArchivoRequest
    {
        public int CodArchivo { get; set; }
        public string Nombre { get; set; }
        public string Origen { get; set; }
        public string Destino { get; set; }
        public int Modulo { get; set; }
    }

    public class SustentoRequest
    {
        public int CodSustento { get; set; }
        public string Nombre { get; set; }
        public int CodLaserFiche { get; set; }
    }

    public class FlujoReporteRequest
    {
        public string Descripcion { get; set; }
    }
}
