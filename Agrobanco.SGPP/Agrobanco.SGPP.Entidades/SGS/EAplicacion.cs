﻿using System;

namespace Agrobanco.SGPP.Entidades.SGS
{
    public class EAplicacion
    {
        public string AplicacionID { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Url { get; set; }
        public string Estado { get; set; }
        public string EstadoDescripcion { get; set; }
        public string UsuarioRegistroID { get; set; }
        public string UsuarioModificacionID { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public string Valor { get; set; }
        public string UsuarioIdRegistro { get; set; }
        public string tipoOperacion { get; set; }
    }
}
