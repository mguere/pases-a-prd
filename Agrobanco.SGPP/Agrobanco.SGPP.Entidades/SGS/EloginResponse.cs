﻿using System.Collections.Generic;

namespace Agrobanco.SGPP.Entidades.SGS
{
    public class EloginResponse
    {
        public EloginResponse()
        {
            eServicioResponse = new EServicioResponse();
            Agencias = new List<EAgencia>();
            vNombre = string.Empty;
            vCargo = string.Empty;
            vAgenciaAsignada = string.Empty;
            vToken = string.Empty;
            vUsuarioWeb = string.Empty;
            vPerfilDescripcion = "Admin";
        }


        public string vToken { get; set; }
        public string vId { get; set; }
        public string vNombre { get; set; }
        public string vCargo { get; set; }
        public string vEmail { get; set; }
        public string vUsuarioWeb { get; set; }
        public string vUsuarioAD { get; set; }
        public string vEstadoAD { get; set; }
        public string vAgenciaAsignada { get; set; }
        public string vDescripcionAgencia { get; set; }
        public string vPassword { get; set; }
        public List<EAgencia> Agencias { get; set; }
        public string vCodFuncionario { get; set; }
        public string vPerfilDescripcion { get; set; }

        public EServicioResponse eServicioResponse { get; set; }


    }
}
