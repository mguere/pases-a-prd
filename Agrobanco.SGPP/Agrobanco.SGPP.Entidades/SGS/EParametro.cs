﻿namespace Agrobanco.SGPP.Entidades.SGS
{
    public class EParametro
    {
        public string ParametroId { get; set; }
        public string IdPadre { get; set; }
        public string Correlativo { get; set; }
        public string Nombre { get; set; }
        public string NombreCorto { get; set; }
        public string Descripcion { get; set; }
        public string Valor { get; set; }
        public string Orden { get; set; }
        public string Estado { get; set; }
    }
}
