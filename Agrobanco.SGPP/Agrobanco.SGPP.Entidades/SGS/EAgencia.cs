﻿namespace Agrobanco.SGPP.Entidades.SGS
{
    public class EAgencia
    {
        public string vCodigo { get; set; }

        public string Valor { get; set; }
        public string Descripcion { get; set; }
        public string NombreRegional { get; set; }
    }
}
