﻿using System.Collections.Generic;

namespace Agrobanco.SGPP.Entidades.SGS
{
    public class ResultadoBusquedaDTO<T>
    {
        public ResultadoBusquedaDTO()
        {
            Lista = new List<T>();
            eServicioResponse = new EServicioResponse();
        }
        public List<T> Lista { get; set; }
        public int TotalRegistros { get; set; }
        public int PaginaActual { get; set; }
        public string Error { get; set; }
        public EServicioResponse eServicioResponse { get; set; }
    }
}
