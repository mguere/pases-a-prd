﻿namespace Agrobanco.SGPP.Entidades.SGS
{
    public class EServicioResponse
    {
        public EServicioResponse()
        {
            Resultado = 1;
        }

        public int Resultado { get; set; }

        public string Mensaje { get; set; }
    }
}
