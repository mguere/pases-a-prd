﻿using System;

namespace Agrobanco.SGPP.Entidades.SGS
{
    public class EPermisosXPerfil
    {
        public string PermisoID { get; set; }
        public string PerfilID { get; set; }
        public string OpcionAplicacionID { get; set; }
        public string EstadoLogico { get; set; }
        public string EstadoLogicoDescripcion { get; set; }
        public string UsuarioRegistroID { get; set; }
        public string UsuarioModificacionID { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public string UsuarioIdRegistro { get; set; }
        public string tipoOperacion { get; set; } //1 registro // 2 actualizar
    }
}
