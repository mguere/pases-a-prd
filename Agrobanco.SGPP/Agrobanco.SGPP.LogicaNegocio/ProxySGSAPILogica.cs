﻿using Agrobanco.SGPP.Comun;
using Agrobanco.SGPP.Entidades.SGS;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SGPP.LogicaNegocio
{
    public class ProxySGSAPILogica
    {
        public ResultadoMantenimientoDTO<EUsuario> LoadMantenimientoUsuario(string usuarioWeb, string Authorization)
        {
            var respuesta = Services.Get<ResultadoMantenimientoDTO<EUsuario>>("api/usuario/LoadMantenimientoUsuario/" + usuarioWeb, Authorization);
            return respuesta;
        }
        public List<EModulo> ObtenerModulos(string idPerfil, string usuarioWeb, string Authorization)
        {
            var respuesta = Services.Get<List<EModulo>>("api/modulo/obtenermodulos/" + idPerfil + "/" + usuarioWeb, Authorization);
            return respuesta;
        }

        public EloginResponse ObtenerUsuario(EUsuario request, string Authorization)
        {
            var respuesta = Services.Post<EloginResponse>(request, "api/usuario/obtenerusuario", "");
            return respuesta;
        }

        public String validate(string Authorization)
        {
            String respuesta = Services.SendRequestv2<String>
                                    ("api/validate/token", null, Authorization, "GET", "application/json");

            return respuesta;
        }
    }
}
