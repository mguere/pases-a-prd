﻿namespace Agrobanco.SGPP.COMUN
{
   
    public enum EAccion
        {
        ENVIAR_ANALISTA_CUMPLIMIENTO = 1,
        EVALUADO = 2,
        DEVOLVER_ANALISTA_COMERCIAL = 3,
        DESCARTAR = 4,
        SOSPECHOSO = 5,
        DEVOLVER_ANALISTA_CUMPLIMIENTO=6,
        INUSUAL=7,
        REASIGNACION=8
    }

}
