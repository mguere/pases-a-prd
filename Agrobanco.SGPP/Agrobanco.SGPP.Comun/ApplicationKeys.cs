﻿
using Agrobanco.SGPP.Control.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SGPP.Comun
{
    public static class ApplicationKeys
    {
        static BaseControl oBaseControl = new BaseControl();

        public static string APILaserfiche = oBaseControl.ObtenerValorParametro("APILaserfiche");
        public static string RegeditPass = oBaseControl.ObtenerValorParametro("RegeditPass");  
        public static string SQLRegeditFolder = oBaseControl.ObtenerValorParametro("SQLRegeditFolder");
        public static string SQLApplicationNameEnableEncrip = oBaseControl.ObtenerValorParametro("SQLApplicationNameEnableEncrip");
        
        public static string CorreoApplicationNameEnableEncrip = oBaseControl.ObtenerValorParametro("CorreoApplicationNameEnableEncrip");  
        public static string CorreoApplicationName = oBaseControl.ObtenerValorParametro("CorreoApplicationName");  



        public static string UrlSSA = oBaseControl.ObtenerValorParametro("UrlSSA");

    }
}
