﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.IO;
using Microsoft.Extensions.Configuration;
using System.Security.Cryptography;

namespace Agrobanco.SGPP.Comun

{
    public static class Util
    {
        public static DateTime ConvertStringToDatetime(string strFecha)
        {
            DateTime tempDate;
            try
            {
               // CultureInfo culture = new CultureInfo("en-US");
                tempDate = DateTime.ParseExact(strFecha, "dd/MM/yyyy", CultureInfo.InvariantCulture);
    //Convert.ToDateTime(strFecha, culture);
            }
            catch (Exception)
            {
                tempDate = DateTime.Now;

            }
            return tempDate;
        }

        public static string ObtenerValueFromKey(string keys)
        {
            var configuration = new ConfigurationBuilder()
                  .SetBasePath(Directory.GetCurrentDirectory())
                  .AddJsonFile("appsettings.json", false)
                  .Build();

            return configuration[keys];
        }

        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //get hash result after compute it
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }
        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source?.IndexOf(toCheck, comp) >= 0;
        }
    }

    public enum TipoAccesoAplicacion
    {
        Url = 1,
        Guid = 2
    }
}

