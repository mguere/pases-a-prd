﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Agrobanco.SGPP.Comun
{
    public class Services
    {
        public static T Post<T>(object data, string rutaMetodo, string token)
        {
            string urlRequest = Util.ObtenerValueFromKey("UrlSGSAPI") + rutaMetodo;
            var _response = new Object();
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );
                var jsonData = JsonConvert.SerializeObject(data);
                // Prueba de obtención de alumno vía HTTP GET
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(urlRequest);
                req.Method = "post";
                req.ContentType = "application/json";

                if (token.Length > 0)
                {
                    req.Headers.Add("Content-Type", "application/json");
                    req.Headers.Add("Authorization", token);
                }

                using (var streamWriter = new StreamWriter(req.GetRequestStream()))
                {
                    string json = jsonData;

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)req.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    _response = streamReader.ReadToEnd();
                }
                T _lista = JsonConvert.DeserializeObject<T>(_response.ToString());

                return _lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static T Get<T>(string metodo, string token)
        {
            string urlRequest = Util.ObtenerValueFromKey("UrlSGSAPI") + metodo;
            var _response = string.Empty;
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );

                // Prueba de obtención de alumno vía HTTP GET
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(urlRequest);
                req.Method = "GET";
                req.ContentType = "application/json";
                req.Timeout = Timeout.Infinite;
                if (token.Length > 0)
                {
                    req.Headers.Add("Content-Type", "application/json");
                    req.Headers.Add("Authorization", token);
                }

                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string _json = reader.ReadToEnd();
                _response = _json;

                T _lista = JsonConvert.DeserializeObject<T>(_response.ToString());

                return _lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public static T SendRequestv2<T>(string metodo, object JSONRequest,
                                      string token, string tipoRequest, string JSONContentType)
        {
            string urlRequest = Util.ObtenerValueFromKey("UrlSGSAPI") + metodo;
            T objResponse = default(T);

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(urlRequest);

                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSONContentType));

                    if (!string.IsNullOrEmpty(token))
                        client.DefaultRequestHeaders.Add("Authorization", token);


                    HttpResponseMessage response = new HttpResponseMessage();
                    if (tipoRequest == "POST")
                    {
                        StringContent sc = new StringContent(JsonConvert.SerializeObject(JSONRequest), Encoding.UTF8, JSONContentType);
                        response = client.PostAsync(urlRequest, sc).Result;
                    }
                    else
                    {
                        response = client.GetAsync(urlRequest).Result;
                    }


                    //var message = response.Content.ReadAsStringAsync();
                    //objResponse = JsonConvert.DeserializeObject<T>(message.Result);

                    //return objResponse;
                    response.EnsureSuccessStatusCode();

                    if (response.IsSuccessStatusCode)
                    {
                        var message = response.Content.ReadAsStringAsync();
                        objResponse = JsonConvert.DeserializeObject<T>(message.Result);

                        return objResponse;

                    }
                    else
                    {



                        //objResponse = JsonConvert.DeserializeObject<T>("401");

                        return objResponse;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
                //objResponse = JsonConvert.DeserializeObject<T>("ex");
                //return objResponse;
            }

        }
    }
}
