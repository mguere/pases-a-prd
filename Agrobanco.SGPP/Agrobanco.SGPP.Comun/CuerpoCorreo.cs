﻿using Agrobanco.SGPP.Comun;
using Agrobanco.SGPP.Entidades;
using System;
using System.IO;

namespace Agrobanco.SGPP.COMUN
{
    public static class CuerpoCorreo
    {
        public static string CuerpoCorreoRegistro(int CodPaseProduccion, 
                                                  string DescripcionTipoFormulario, 
                                                  string Descripcion, 
                                                  string Ticket, 
                                                  string path)
        {
            string body = string.Empty;
            string plantilla = Path.Combine(path, "Plantillas\\RegistroPase.html");

            try
            {
                using (StreamReader reader = new StreamReader(plantilla))
                {
                    body = reader.ReadToEnd();
                }

                body = body.Replace("{CodPaseProduccion}", CodPaseProduccion.ToString());
                body = body.Replace("{DescripcionTipoFormulario}", DescripcionTipoFormulario);
                body = body.Replace("{Descripcion}", Descripcion);
                body = body.Replace("{Ticket}", Ticket);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return body;
        }
        public static string CuerpoCorreoDerivacion(int CodPaseProduccion,
                                                        string DescripcionTipoFormulario,
                                                        string Descripcion,
                                                        string Ticket,
                                                        string path)
        {
            string body = string.Empty;
            string plantilla = Path.Combine(path, "Plantillas\\DerivacionPase.html");

            try
            {
                using (StreamReader reader = new StreamReader(plantilla))
                {
                    body = reader.ReadToEnd();
                }

                body = body.Replace("{CodPaseProduccion}", CodPaseProduccion.ToString());
                body = body.Replace("{DescripcionTipoFormulario}", DescripcionTipoFormulario);
                body = body.Replace("{Descripcion}", Descripcion);
                body = body.Replace("{Ticket}", Ticket);
                body = body.Replace("{LinkAccesoSGS}", ApplicationKeys.UrlSSA);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return body;
        }

        public static string CuerpoCorreoRechazo(PaseBE datosPase, string descripcionTipoPase, string path)
        {
            string body = string.Empty;
            string plantilla = Path.Combine(path, "Plantillas\\RechazoPase.html");

            try
            {
                using (StreamReader reader = new StreamReader(plantilla))
                {
                    body = reader.ReadToEnd();
                }

                body = body.Replace("{CodPaseProduccion}", datosPase.CodPaseProduccion.ToString());
                body = body.Replace("{DescripcionTipoFormulario}", descripcionTipoPase);
                body = body.Replace("{Descripcion}", datosPase.Descripcion);
                body = body.Replace("{Ticket}", datosPase.Ticket);
                body = body.Replace("{LinkAccesoSGS}", ApplicationKeys.UrlSSA);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return body;
        }

        public static string CuerpoCorreoAprobacion(PaseBE oPase, string path, int estado, string EnviadoA, bool bAnalista)
        {
            string body = string.Empty;
            string plantilla = Path.Combine(path, "Plantillas\\Aprobacion.html");

            try
            {
                using (StreamReader reader = new StreamReader(plantilla))
                {
                    body = reader.ReadToEnd();
                }

                string TipoPaseDesc = string.Empty;
                string contenido = string.Empty;


                switch (oPase.TipoPase)
                {
                    case 1:
                        TipoPaseDesc = TipoPaseDescripcion.AplicacionesWeb;
                        break;
                    case 2:
                        TipoPaseDesc = TipoPaseDescripcion.AS400;
                        break;
                    case 3:
                        TipoPaseDesc = TipoPaseDescripcion.PaginasWeb;
                        break;
                    case 4:
                        TipoPaseDesc = TipoPaseDescripcion.Spring;
                        break;
                }

                if (estado == EstadosPase.DerCalidad && bAnalista)
                {
                    contenido = "Su formulario número <b>" + oPase.CodPaseProduccion + "</b> de pase a producción de <b>" + TipoPaseDesc + "</b> ha sido aprobado y enviado a " + EnviadoA + "<br> <br> <br>" +
                                    "<b> Proyecto: </b>" + oPase.Descripcion + "<br> <br>" +
                                    "<b> Nro. Rquerimiento: </b>" + oPase.Ticket;

                }
                else if (estado == EstadosPase.DerCalidad && !bAnalista)
                {
                    contenido = "El formulario número <b>" + oPase.CodPaseProduccion + "</b> de pase a producción de <b>" + TipoPaseDesc + "</b> ha sido revisado por el ejecutivo de sistemas y enviado para su aprobación. <br> <br> <br>" +
                                    "<b> Proyecto: </b>" + oPase.Descripcion + "<br> <br>" +
                                    "<b> Nro. Rquerimiento: </b>" + oPase.Ticket + "</br> <br>" +
                                    "<b> Consultar: </b>" + ApplicationKeys.UrlSSA;
                }else if (estado == EstadosPase.DerSoporte && bAnalista)
                {
                    contenido = "Su formulario número <b>" + oPase.CodPaseProduccion + "</b> de pase a producción de <b>" + TipoPaseDesc + "</b> ha sido aprobado y enviado al Operador. <br> <br> <br>" +
                                "<b> Proyecto: </b>" + oPase.Descripcion + "<br> <br>" +
                                "<b> Nro. Rquerimiento: </b>" + oPase.Ticket;
                }else if (estado == EstadosPase.DerSoporte && !bAnalista)
                {
                    contenido = "El formulario número <b>" + oPase.CodPaseProduccion + "</b> de pase a producción de <b>" + TipoPaseDesc + "</b> ha sido aprobado por el control de calidad y enviado para su despliegue. <br> <br> <br>" +
                                    "<b> Proyecto: </b>" + oPase.Descripcion + "<br> <br>" +
                                    "<b> Nro. Rquerimiento: </b>" + oPase.Ticket + "</br> <br>" +
                                    "<b> Consultar: </b>" + ApplicationKeys.UrlSSA;
                }else if (estado == EstadosPase.Aprobado && !bAnalista)
                {
                    contenido = "Su formulario número <b>" + oPase.CodPaseProduccion + "</b> de pase a producción de <b>" + TipoPaseDesc + "</b> ha PROCESADO por el OPERADOR. <br> <br> <br>" +
                                    "<b> Proyecto: </b>" + oPase.Descripcion + "<br> <br>" +
                                    "<b> Nro. Rquerimiento: </b>" + oPase.Ticket;
                }

                body = body.Replace("{XContenidoX}", contenido);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return body;
        }

        public static string CuerpoCorreoAnulacion(PaseBE datosPase, string descripcionTipoPase, string path)
        {
            string body = string.Empty;
            string plantilla = Path.Combine(path, "Plantillas\\Anulacion.html");

            try
            {
                using (StreamReader reader = new StreamReader(plantilla))
                {
                    body = reader.ReadToEnd();
                }

                body = body.Replace("{CodPaseProduccion}", datosPase.CodPaseProduccion.ToString());
                body = body.Replace("{DescripcionTipoFormulario}", descripcionTipoPase);
                body = body.Replace("{Descripcion}", datosPase.Descripcion);
                body = body.Replace("{Ticket}", datosPase.Ticket);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return body;
        }
    }
}
