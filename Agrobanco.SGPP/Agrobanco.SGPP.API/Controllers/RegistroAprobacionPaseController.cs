﻿using Agrobanco.SGPP.API.Models;
using Agrobanco.SGPP.Comun.Laserfiche;
using Agrobanco.SGPP.Control.Filters;
using Agrobanco.SGPP.Entidades;
using Agrobanco.SGPP.Entidades.RegistroAprobacionPase;
using Agrobanco.SGPP.Entidades.RegistroPase;
using Agrobanco.SGPP.LogicaNegocio;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Agrobanco.SGPP.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/registro-aprobacion-pase")]
    [ApiController]
    public class RegistroAprobacionPaseController : ControllerBase
    {
        private readonly RegistroAprobacionPaseLogica regAprobPaseLogica;
        public RegistroAprobacionPaseController(
                IHostingEnvironment env,
                ILogger<RegistroAprobacionPaseController> logger)
        {
            regAprobPaseLogica = new RegistroAprobacionPaseLogica(env, logger);

        }

        [HttpPost("listar")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult RegAprobPaseLista([Required][FromHeader(Name = "Token")] string token, [FromBody] RegAprobPaseFilterRequest oRequest)
        {
            try
            {
                var Resultado = regAprobPaseLogica.ListarPases(oRequest);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("listarEstadosPase")]
        [ProducesResponseType(200, Type = typeof(List<EstadosPaseBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult Estados([Required][FromHeader(Name = "Token")] string token)
        {
            try
            {
                var listaResultado = regAprobPaseLogica.ListarEstados();

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("listarTipoPlataforma")]
        [ProducesResponseType(200, Type = typeof(List<TiposPlataformaBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult TipoPlataforma([Required][FromHeader(Name = "Token")] string token)
        {
            try
            {
                var listaResultado = regAprobPaseLogica.ListarTipoPlataforma();

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("Librerias/{Tipo}")]
        [ProducesResponseType(200, Type = typeof(List<LibreriaBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult ListarLibrerias([Required][FromHeader(Name = "token")] string token, int Tipo)
        {
            try
            {
                var result = regAprobPaseLogica.ListarLibrerias(Tipo);
                return StatusCode(200, result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("RegistrarPase")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccessDetalle))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        
        public async Task<object> RegistrarPase([Required][FromHeader(Name = "Token")] string token, [FromBody] RegPaseRequest request)
        {
            try
            {
                ResponseSuccessDetalle response = new ResponseSuccessDetalle();
                var codPaseProduccion = await regAprobPaseLogica.RegistrarPase(request);
                response.resultado = true;
                response.mensaje = $"Se ha ingresado correctamente el pase a producción {codPaseProduccion}.";  
                response.codigo = codPaseProduccion.ToString();
                return StatusCode(200, response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("ActualizarPase")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccessDetalle))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]

        public async Task<object> ActualizarPase([Required][FromHeader(Name = "Token")] string token, [FromBody] RegPaseRequest request)
        {
            try
            {
                ResponseSuccessDetalle response = new ResponseSuccessDetalle();
                var resultado = await regAprobPaseLogica.ActualizarPase(request);
                response.resultado = resultado;
                response.mensaje = "Se ha actualizado correctamente el pase a producción.";
                response.codigo = request.CodPaseProduccion.ToString();
                return StatusCode(200, response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("Analistas")]
        [ProducesResponseType(200, Type = typeof(List<LibreriaBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult ListarAnalista([Required][FromHeader(Name = "token")] string token)
        {
            try
            {
                var result = regAprobPaseLogica.ListarAnalista();
                return StatusCode(200, result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet("obtenerPase/{codPase}")]
        [ProducesResponseType(200, Type = typeof(PaseBE))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult ObtenerPase([Required][FromHeader(Name = "Token")] string token, int codPase)
        {
            try
            {
                var result = regAprobPaseLogica.ObtenerPase(codPase);

                return StatusCode(200, result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet("obtenerDatosFlujo/{codPase}")]
        [ProducesResponseType(200, Type = typeof(List<string>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult ObtenerDatosFlujo([Required][FromHeader(Name = "Token")] string token, int codPase)
        {
            try
            {
                var result = regAprobPaseLogica.ObtenerDatosFlujo(codPase);

                return StatusCode(200, result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet("ObtenerComentariosFlujo/{codPase}")]
        [ProducesResponseType(200, Type = typeof(ComentarioFlujoBE))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult ObtenerComentariosFlujo([Required][FromHeader(Name = "Token")] string token, int codPase)
        {
            try
            {
                var result = regAprobPaseLogica.ObtenerComentariosFlujo(codPase);

                return StatusCode(200, result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("archivos/{codPase}")]
        [ProducesResponseType(200, Type = typeof(List<ArchivoBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult ListarArchivos(int codPase)
        {
            try
            {
                var result = regAprobPaseLogica.ListarArchivos(codPase);
                return StatusCode(200, result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("objetos/{codPase}")]
        [ProducesResponseType(200, Type = typeof(List<ObjetoBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult ListarObjetos(int codPase)
        {
            try
            {
                var result = regAprobPaseLogica.ListarObjetos(codPase);
                return StatusCode(200, result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("observaciones/{codPase}")]
        [ProducesResponseType(200, Type = typeof(List<ObjetoBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult ObtenerObservaciones([Required][FromHeader(Name = "Token")] string token, int codPase)
        {
            try
            {
                var result = regAprobPaseLogica.ObtenerObservaciones(codPase);
                return StatusCode(200, result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("sustentos/{codPase}")]
        [ProducesResponseType(200, Type = typeof(List<Sustento>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public async Task<IActionResult> ListarSustentos(int codPase)
        {
            try
            {
                var result = await regAprobPaseLogica.ListarSustentos(codPase);
                return StatusCode(200, result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("descargar/{idLaserfiche}")]
        [ValidateAuthorizationRequest]
        public IActionResult Descargar(int idLaserfiche)
        {
            try
            {
                LaserficheProxy laserfiche = new LaserficheProxy();
                Documento doc = laserfiche.ConsultarDocumentoBytes(idLaserfiche);

                string fileName = doc.NombreDocumento;
                return File(doc.ArrayBytes, MediaTypeNames.Application.Octet, fileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("ObtenerInformacionSoporte/{tipoPase}")]
        [ProducesResponseType(200, Type = typeof(List<InformacionSoporteBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult ObtenerInformacionSoporte([Required][FromHeader(Name = "Token")] string token, int tipoPase)
        {
            try
            {
                var result = regAprobPaseLogica.ObtenerInformacionSoporte(tipoPase);
                return StatusCode(200, result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("AprobarPase")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccessDetalle))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public async Task<object> AprobarPase([Required][FromHeader(Name = "Token")] string token, [FromBody] ActualizacionPase request)
        {
            try
            {
                ResponseSuccessDetalle response = new ResponseSuccessDetalle();
                var resultado = await regAprobPaseLogica.AprobarPase(request);
                response.resultado = resultado;
                response.mensaje = "Se ha aprobado correctamente el pase a producción.";
                //response.codigo = request.CodPaseProduccion.ToString();
                return StatusCode(200, response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("RechazarPase")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccessDetalle))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public async Task<object> RechazarPase([Required][FromHeader(Name = "Token")] string token, [FromBody] ActualizacionPase request)
        {
            try
            {
                ResponseSuccessDetalle response = new ResponseSuccessDetalle();
                var resultado = await regAprobPaseLogica.RechazarPase(request);
                response.resultado = resultado.Procesado;
                response.mensaje = resultado.Mensaje;
                return StatusCode(200, response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("AnularPase")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccessDetalle))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public async Task<object> AnularPase([Required][FromHeader(Name = "Token")] string token, [FromBody] ActualizacionPase request)
        {
            try
            {
                ResponseSuccessDetalle response = new ResponseSuccessDetalle();
                var resultado = await regAprobPaseLogica.AnularPase(request);
                response.resultado = resultado.Procesado;
                response.mensaje = resultado.Mensaje;
                return StatusCode(200, response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
