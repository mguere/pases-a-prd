﻿using Agrobanco.SGPP.Control.Security;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Agrobanco.SGPP.Control.Base
{
    public class BaseControl
    {
        private readonly ITokenControl _tokencontrol;

        public BaseControl()
        {
            _tokencontrol = new TokenControl();
        }

        #region token
        public void ValidarTokenSesion(string token)
        {
            try
            {
                var paramkeytoken = ObtenerValorParametro(ConstantesParametros.TokenClave);
                _tokencontrol.IsTokenJWTValid(paramkeytoken, token);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string ObtenerValorClaimToken(string token, string tipoclaim)
        {
            try
            {
                token = token.Contains("Bearer ") ? token.Replace("Bearer ", "") : token;

                var paramkeytoken = ObtenerValorParametro(ConstantesParametros.TokenClave);

                var valorclaim = _tokencontrol.GetClaimValueByToken(paramkeytoken, tipoclaim, token);

                return valorclaim;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region Parametros
        public string ObtenerValorParametro(string parametro)
        {
            var parametros = ObtenerParametros();

            var valorParametro = parametros.Find(x => x.llave == parametro) == null ? "" : parametros.Find(x => x.llave == parametro).valor;
            return valorParametro;
        }

        private List<ParametroControlDto> ObtenerParametros()
        {
            try
            {
                var configuration = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json", false)
                   .Build();

                var responseParametros = new List<ParametroControlDto> {
                                                        new ParametroControlDto {llave = "TokenClave" , valor = configuration["TokenClave"] },
                                                        new ParametroControlDto {llave = "TokenMinutos" , valor = configuration["TokenMinutos"] },
                                                        new ParametroControlDto {llave = "AppKey" , valor = configuration["AppKey"] },
                                                        new ParametroControlDto {llave = "AppCode" , valor = configuration["AppCode"] },

                                                        new ParametroControlDto {llave = "SQLApplicationNameEnableEncrip" , valor = configuration["SQLApplicationNameEnableEncrip"] },
                                                        new ParametroControlDto {llave = "SQLRegeditFolder" , valor = configuration["SQLRegeditFolder"] },
                                                        new ParametroControlDto {llave = "RegeditPass" , valor = configuration["RegeditPass"] },
                                                        new ParametroControlDto {llave = "APILaserfiche" , valor = configuration["APILaserfiche"] },

                                                        
                                                        new ParametroControlDto {llave = "CorreoApplicationNameEnableEncrip" , valor = configuration["CorreoApplicationNameEnableEncrip"] },
                                                        new ParametroControlDto {llave = "CorreoApplicationName" , valor = configuration["CorreoApplicationName"] },
                                                        new ParametroControlDto {llave = "UrlSSA" , valor = configuration["UrlSSA"] },

                };
                return responseParametros;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
