﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.LaserFiche.API.Models
{
    public class ImagenesModel
    {
        public bool ResultadoOK { get; set; }
        public String ResultadoMensaje { get; set; }
        public string Folder { get; set; }
        public List<ItemImagen> ListaImagenes;
    }

    public class ItemImagen
    {
        public int CodigoImagen { get; set; }
        public int Tipo { get; set; }
        public string Extension { get; set; }
        public string Nombre { get; set; }

        public string ArchivoBytes { get; set; }
        public int CodigoLaser { get; set; }

        public bool ResultadoOK { get; set; }
        public String ResultadoMensaje { get; set; }



    }
}