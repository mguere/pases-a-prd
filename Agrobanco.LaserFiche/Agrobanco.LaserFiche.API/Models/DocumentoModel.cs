﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.LaserFiche.API.Models
{
    public class DocumentoModel
    {
        public bool ResultadoOK { get; set; }
        public String ResultadoMensaje { get; set; }
        public string Folder { get; set; }
        public List<DocumentoItem> ListaDocumentos;
    }

    public class DocumentoItem
    {
        public int CodigoLaserfiche { get; set; }
        public string CodigoLaserficheAnidados { get; set; }
        public string ArchivoBytes { get; set; }
        public string Extension { get; set; }
        public string Nombre { get; set; }
        public string NombreDocumento { get; set; }
        public string Folder { get; set; }
        public string Tamanio { get; set; }
        public string ContentType { get; set; }

        public bool ResultadoOK { get; set; }
        public String ResultadoMensaje { get; set; }
    }
}