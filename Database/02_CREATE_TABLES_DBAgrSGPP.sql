USE DBAgrSGPP
GO

IF OBJECT_ID('dbo.Parametro', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.Parametro;
END
GO

IF OBJECT_ID('dbo.Archivo', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.Archivo;
END
GO

IF OBJECT_ID('dbo.DetalleEstado', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.DetalleEstado;
END
GO

IF OBJECT_ID('dbo.Objeto', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.Objeto;
END
GO

IF OBJECT_ID('dbo.Sustento', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.Sustento;
END
GO

IF OBJECT_ID('dbo.PaseProduccion', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.PaseProduccion;
END
GO

IF OBJECT_ID('dbo.Usuario', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.Usuario;
END
GO

CREATE TABLE dbo.Parametro
(
	CodParametro INT IDENTITY(1,1) NOT NULL,
	Grupo INT NOT NULL,
	Parametro INT NOT NULL,
	Descripcion VARCHAR(200) NULL,
	Valor VARCHAR(4000) NULL,
	Referencia VARCHAR(200) NULL,
	Orden INT NULL,
	Estado bit NULL
)
GO

ALTER TABLE dbo.Parametro
ADD CONSTRAINT PK_Parametro PRIMARY KEY (CodParametro)
GO

CREATE TABLE dbo.Archivo
(
	CodArchivo INT IDENTITY(1,1) NOT NULL,
	CodPaseProduccion INT NOT NULL,
	Nombre VARCHAR(150) NOT NULL,
	Origen VARCHAR(500) NULL,
	Destino VARCHAR(500) NULL,
	Modulo INT NULL,
	UsuarioRegistro VARCHAR(50) NOT NULL,
	UsuarioModificacion VARCHAR(50) NULL,
	FechaRegistro DATETIME NOT NULL,
	FechaModificacion DATETIME NULL
)
GO

ALTER TABLE dbo.Archivo
ADD CONSTRAINT PK_Archivo PRIMARY KEY (CodArchivo)
GO

CREATE TABLE dbo.DetalleEstado
(
	CodDetalleEstado INT IDENTITY(1,1) NOT NULL,
	CodPaseProduccion INT NOT NULL,
	Estado INT NOT NULL,
	Comentario VARCHAR(1500) NOT NULL,
	UsuarioRegistro VARCHAR(50) NOT NULL,
	UsuarioModificacion VARCHAR(50) NULL,
	FechaRegistro DATETIME NOT NULL,
	FechaModificacion DATETIME NULL,
	CodUsuario INT NOT NULL
)
GO

ALTER TABLE dbo.DetalleEstado
ADD CONSTRAINT PK_DetalleEstado PRIMARY KEY (CodDetalleEstado)
GO

CREATE TABLE dbo.Objeto(
	CodObjeto INT IDENTITY(1,1) NOT NULL,
	CodPaseProduccion INT NOT NULL,
	Tipo INT NOT NULL,
	Nombre VARCHAR(150) NOT NULL,
	Descripcion VARCHAR(1500) NOT NULL,
	UsuarioRegistro VARCHAR(50) NOT NULL,
	UsuarioModificacion VARCHAR(50) NULL,
	FechaRegistro DATETIME NOT NULL,
	FechaModificacion DATETIME NULL
)
GO

ALTER TABLE dbo.Objeto
ADD CONSTRAINT PK_Objeto PRIMARY KEY (CodObjeto)
GO

CREATE TABLE dbo.Sustento
(
	CodSustento INT IDENTITY(1,1) NOT NULL,
	CodPaseProduccion INT NOT NULL,
	Nombre VARCHAR(150) NOT NULL,
	Activo bit NOT NULL,
	CodLaserFiche INT NOT NULL,
	UsuarioRegistro VARCHAR(50) NOT NULL,
	UsuarioModificacion VARCHAR(50) NULL,
	FechaRegistro DATETIME NOT NULL,
	FechaModificacion DATETIME NULL
)
GO

ALTER TABLE dbo.Sustento
ADD CONSTRAINT PK_Sustento PRIMARY KEY (CodSustento)
GO

CREATE TABLE dbo.Usuario
(
	CodUsuario INT IDENTITY(1,1) NOT NULL,
	Perfil VARCHAR(5) NOT NULL,
	DescripcionPerfil VARCHAR(200) NOT NULL,
	CorreoElectronico VARCHAR(100) NOT NULL,
	UsuarioWeb VARCHAR(20) NOT NULL,
	NombresApellidos VARCHAR(200) NOT NULL,
	Estado bit NOT NULL,
	UsuarioRegistro VARCHAR(50) NOT NULL,
	UsuarioModificacion VARCHAR(50) NULL,
	FechaRegistro DATETIME NOT NULL,
	FechaModificacion DATETIME NULL
)
GO

ALTER TABLE dbo.Usuario
ADD CONSTRAINT PK_USUARIO_CODUSUARIO PRIMARY KEY (CodUsuario)
GO

CREATE TABLE dbo.PaseProduccion
(
	CodPaseProduccion INT IDENTITY(1,1) NOT NULL,
	TipoPase INT NOT NULL,
	FechaPase date NOT NULL,
	Ticket VARCHAR(100) NOT NULL,
	Descripcion VARCHAR(1500) NOT NULL,
	UbicacionFuente VARCHAR(500) NULL,
	PaseInterno bit NULL,
	MotivoPase INT NULL,
	Compilacion INT NULL,
	CondicionesEspeciales VARCHAR(1500) NULL,
	LibreriaFuente INT NULL,
	LibreriaObjetoOtros INT NULL,
	LibreriaObjetoPFyLF INT NULL,
	Analista INT NOT NULL,
	Estado INT NOT NULL,
	UsuarioRegistro VARCHAR(50) NOT NULL,
	UsuarioModificacion VARCHAR(50) NULL,
	FechaRegistro DATETIME NOT NULL,
	FechaModificacion DATETIME NULL
)
GO

ALTER TABLE dbo.PaseProduccion
ADD CONSTRAINT PK_PaseProduccion PRIMARY KEY (CodPaseProduccion)
GO


ALTER TABLE dbo.Archivo
ADD CONSTRAINT FK_Archivo_PaseProduccion FOREIGN KEY (CodPaseProduccion) REFERENCES dbo.PaseProduccion (CodPaseProduccion)
GO

ALTER TABLE dbo.DetalleEstado  
ADD CONSTRAINT FK_DetalleEstado_PaseProduccion FOREIGN KEY(CodPaseProduccion) REFERENCES dbo.PaseProduccion (CodPaseProduccion)
GO

ALTER TABLE dbo.DetalleEstado  
ADD CONSTRAINT FK_DetalleEstado_Usuario FOREIGN KEY(CodUsuario) REFERENCES dbo.Usuario (CodUsuario)
GO

ALTER TABLE dbo.Objeto  
ADD CONSTRAINT FK_Objeto_PaseProduccion FOREIGN KEY(CodPaseProduccion) REFERENCES dbo.PaseProduccion (CodPaseProduccion)
GO

ALTER TABLE dbo.PaseProduccion  
ADD CONSTRAINT FK_PaseProduccion_Usuario FOREIGN KEY(Analista) REFERENCES dbo.Usuario (CodUsuario)
GO

ALTER TABLE dbo.Sustento  
ADD CONSTRAINT FK_Sustento_PaseProduccion FOREIGN KEY(CodPaseProduccion) REFERENCES dbo.PaseProduccion (CodPaseProduccion)
GO

CREATE TYPE TObjetos AS TABLE (
	Tipo                INT,
	Nombre              VARCHAR (150),
	Descripcion         VARCHAR (1500)
)
GO

CREATE TYPE TArchivos AS TABLE (
	Nombre              VARCHAR (150),
	Origen              VARCHAR (500),
	Destino             VARCHAR (500),
	Modulo              INT
)
GO

CREATE TYPE TSustentos AS TABLE (
	Nombre              VARCHAR (150),
	CodLaserFiche       INT
)
GO


