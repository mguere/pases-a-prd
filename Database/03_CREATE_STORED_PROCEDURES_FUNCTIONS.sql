USE DBAgrSGPP
GO
DROP PROCEDURE IF EXISTS dbo.SP_PASE_LISTAR
GO

 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 15/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: KARINA TORRES
-- STORE PROCEDURE			: SP_PASE_LISTAR
-- FUNCIONALIDAD			: Obtener la lista pases a producción
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_PASE_LISTAR] 
@TipoPlataforma int = -1,
@CodEstado int = -1,
@FechaRegistroIni DATETIME,
@FechaRegistroFin DATETIME
AS
BEGIN   


	SELECT
		P.CodPaseProduccion AS IdPases,
		U.NombresApellidos AS Nombres,
		P.Descripcion,
		U.NombresApellidos AS CreadoPor,
		ISNULL(DE.NombresApellidos,'') AS AprobadorPor ,
		P.FechaRegistro ,
		ES.Valor AS CodEstado,
		(CASE P.PaseInterno WHEN 1 THEN 'SI' WHEN 0 THEN 'NO' ELSE '' END) as Pase		
	FROM   
		PaseProduccion P WITH(NOLOCK) 		
		INNER JOIN Usuario U WITH(NOLOCK) ON U.CodUsuario = P.Analista
		INNER JOIN PARAMETRO ES WITH(NOLOCK) ON ES.Parametro = P.Estado  AND ES.Grupo = 200
		OUTER APPLY (
			select top 1 ua.NombresApellidos
				from DetalleEstado as de with(nolock)
				inner join Usuario UA WITH(NOLOCK) ON de.CodUsuario =  UA.CodUsuario 
				where de.CodPaseProduccion = p.CodPaseProduccion
				and de.Estado = 2
				order by de.CodDetalleEstado desc
		) DE
	WHERE		
		(@CodEstado = -1 OR P.Estado = @CodEstado)
		AND (@TipoPlataforma = -1 OR P.TipoPase = @TipoPlataforma)		
		AND ((@FechaRegistroIni IS NULL OR @FechaRegistroIni = '') OR CAST(P.FechaRegistro AS DATE) >= CAST(@FechaRegistroIni AS DATE))
		AND ((@FechaRegistroFin IS NULL OR @FechaRegistroFin = '') OR CAST(P.FechaRegistro AS DATE) <= CAST(@FechaRegistroFin AS DATE))
	ORDER BY P.CodPaseProduccion DESC
		
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_ESTADO_LISTAR
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 15/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: KARINA TORRES
-- STORE PROCEDURE			: SP_ESTADO_LISTAR
-- FUNCIONALIDAD			: Obtener la lista estados de pases a produccion
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_ESTADO_LISTAR]
AS  
BEGIN 
	 SELECT Parametro Id 
		  ,Referencia AS CodEstado
		  , Valor AS Descripcion
	 FROM Parametro  
	 WHERE Grupo = 200
	 ORDER BY Orden
END 
GO
DROP PROCEDURE IF EXISTS dbo.SP_TIPO_PLATAFORMA_LISTAR
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 15/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: KARINA TORRES
-- STORE PROCEDURE			: SP_TIPO_PLATAFORMA_LISTAR
-- FUNCIONALIDAD			: Obtener la lista tipos de plataforma de pases a produccion
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_TIPO_PLATAFORMA_LISTAR]    
AS  
BEGIN  
	 SELECT Parametro AS CodTipoPlataforma
		  , Descripcion 
	 FROM Parametro  
	 WHERE Grupo = 100
	 ORDER BY Orden 
END  
GO

DROP PROCEDURE IF EXISTS dbo.SP_PASE_INSERTAR
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 15/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: FERNANDO VASQUEZ
-- STORE PROCEDURE			: SP_PASE_INSERTAR
-- FUNCIONALIDAD			: Inserta la información de un pase a producción
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_PASE_INSERTAR] 
	@TipoPase              INT,
	@FechaPase             DATE,
	@Ticket                VARCHAR (100),
	@Descripcion           VARCHAR (1500),
	@UbicacionFuente       VARCHAR (500),
	@PaseInterno           BIT,
	@MotivoPase            INT,
	@Compilacion           INT,
	@CondicionesEspeciales VARCHAR (1500),
	@LibreriaFuente        INT,
	@LibreriaObjetoOtros   INT,
	@LibreriaObjetoPFyLF   INT,
	@Analista              INT,
	@UsuarioRegistro       VARCHAR(50),
	@Comentario            VARCHAR (1500),
	@TObjetos 		  	   TObjetos READONLY,
	@TArchivos	 		   TArchivos READONLY,
	@TSustentos 		   TSustentos READONLY,
	@CodPaseProduccion	   INT OUTPUT
AS
BEGIN   
	SET NOCOUNT ON

	DECLARE @P_CodPaseProduccion INT;
	DECLARE @Estado_DerEjecutivo INT = 1;
	DECLARE @Activo BIT = 1;
	DECLARE @Mensaje VARCHAR(MAX);

	BEGIN TRY
		BEGIN TRAN
			INSERT INTO dbo.PaseProduccion (
						TipoPase, 
						FechaPase, 
						Ticket, 
						Descripcion, 
						UbicacionFuente, 
						PaseInterno, 
						MotivoPase, 
						Compilacion, 
						CondicionesEspeciales, 
						LibreriaFuente, 
						LibreriaObjetoOtros, 
						LibreriaObjetoPFyLF, 
						Analista, 
						Estado,
						UsuarioRegistro,
						FechaRegistro
					)
			VALUES (
						@tipopase, 
						@fechapase, 
						@ticket, 
						@descripcion, 
						@ubicacionfuente, 
						@paseinterno, 
						@motivopase, 
						@compilacion, 
						@condicionesespeciales, 
						@libreriafuente, 
						@libreriaobjetootros, 
						@libreriaobjetopfylf, 
						@analista, 
						@Estado_DerEjecutivo,
						@usuarioregistro, 
						GETDATE()
					)
			SET @P_CodPaseProduccion = SCOPE_IDENTITY()

			/* Insertando el Detalle del Estado*/
			INSERT INTO dbo.DetalleEstado (
						CodPaseProduccion, 
						Estado, 
						Comentario, 
						UsuarioRegistro, 
						FechaRegistro, 
						CodUsuario
					)
			VALUES (
						@P_CodPaseProduccion, 
						@Estado_DerEjecutivo, 
						@comentario, 
						@usuarioregistro, 
						GETDATE(),
						@analista
					)
			/* Insertando Objetos */
			INSERT INTO dbo.Objeto (
						CodPaseProduccion, 
						Tipo, 
						Nombre, 
						Descripcion, 
						UsuarioRegistro,
						FechaRegistro
						)
			SELECT
						@P_CodPaseProduccion, 
						Tipo, 
						Nombre, 
						Descripcion, 
						@UsuarioRegistro, 
						GETDATE()
			FROM @TObjetos
			
			/* Insertando Archivos*/
			INSERT INTO dbo.Archivo (
						CodPaseProduccion, 
						Nombre, 
						Origen, 
						Destino, 
						Modulo, 
						UsuarioRegistro,
						FechaRegistro
						)
			SELECT
						@P_CodPaseProduccion, 
						Nombre, 
						Origen, 
						Destino, 
						Modulo, 
						@UsuarioRegistro,
						GETDATE()
			FROM @TArchivos

			SET @CodPaseProduccion = @P_CodPaseProduccion
		COMMIT TRAN
	END TRY
	BEGIN CATCH 
		ROLLBACK 
		SET @Mensaje = ERROR_MESSAGE()
		RAISERROR(@Mensaje,16,-1)
	END CATCH
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_SUSTENTO_INSERTAR
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 15/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: FERNANDO VASQUEZ
-- STORE PROCEDURE			: SP_SUSTENTO_INSERTAR
-- FUNCIONALIDAD			: Inserta los sustentos de un pase a producción
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_SUSTENTO_INSERTAR] 
	@CodPaseProduccion     INT,
	@UsuarioRegistro       VARCHAR(50),
	@TSustentos 		   TSustentos READONLY,
	@Confirmacion		   INT OUTPUT
AS
BEGIN   
	SET NOCOUNT ON

	DECLARE @Activo BIT = 1;
	DECLARE @Mensaje VARCHAR(MAX);

	BEGIN TRY
		BEGIN TRAN
			
			INSERT INTO dbo.Sustento (
						CodPaseProduccion, 
						Nombre, 
						Activo, 
						CodLaserFiche, 
						UsuarioRegistro, 
						FechaRegistro
					)
			SELECT
						@CodPaseProduccion, 
						Nombre, 
						@Activo, 
						CodLaserFiche, 
						@UsuarioRegistro, 
						GETDATE()
			FROM @TSustentos

			SET @Confirmacion = 1
		COMMIT TRAN
	END TRY
	BEGIN CATCH 
		ROLLBACK 
		SET @Mensaje = ERROR_MESSAGE()
		SET @Confirmacion = 0
		RAISERROR(@Mensaje,16,-1)
	END CATCH
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_PASE_OBTENER_POR_CODIGO
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 15/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: FERNANDO VASQUEZ
-- STORE PROCEDURE			: SP_PASE_OBTENER_POR_CODIGO
-- FUNCIONALIDAD			: Obtener el pase de producción por código
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_PASE_OBTENER_POR_CODIGO]
	@CodPaseProduccion INT
AS
BEGIN   

	DECLARE @Grupo_TipoPlataforma INT = 100
	DECLARE @Grupo_Estado INT = 200

	SELECT
		PP.CodPaseProduccion,
		PP.Analista,
		PP.Ticket,
		PP.TipoPase,
		PP.Descripcion,
		ISNULL(PP.UbicacionFuente,'') AS UbicacionFuente,
		PP.PaseInterno,
		ISNULL(PP.LibreriaFuente,0) AS LibreriaFuente,
		ISNULL(PP.LibreriaObjetoOtros,0) AS LibreriaObjetoOtros,
		ISNULL(PP.LibreriaObjetoPFyLF,0) AS LibreriaObjetoPFyLF,
		ISNULL(PP.MotivoPase,0) AS MotivoPase,
		PP.Estado AS Estado,
		DE.Comentario
	FROM PaseProduccion PP WITH(NOLOCK) 
	CROSS APPLY(
		select top 1 Comentario
			from DetalleEstado with(nolock)
			where CodPaseProduccion = PP.CodPaseProduccion
			and Estado = 1
			order by CodDetalleEstado desc
	) DE
	WHERE PP.CodPaseProduccion = @CodPaseProduccion
		
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_OBJETO_LISTAR_POR_CODIGO
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 15/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: FERNANDO VASQUEZ
-- STORE PROCEDURE			: SP_OBJETO_LISTAR_POR_CODIGO
-- FUNCIONALIDAD			: Listar los objetos del pase a producción
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_OBJETO_LISTAR_POR_CODIGO] 
	@CodPaseProduccion INT
AS
BEGIN   

	SELECT 
		O.CodObjeto,
		O.tipo,
		O.Nombre,
		O.Descripcion
	FROM dbo.Objeto O
	WHERE O.CodPaseProduccion = @CodPaseProduccion

END
GO

DROP PROCEDURE IF EXISTS dbo.SP_ARCHIVO_LISTAR_POR_CODIGO
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 15/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: FERNANDO VASQUEZ
-- STORE PROCEDURE			: SP_ARCHIVO_LISTAR_POR_CODIGO
-- FUNCIONALIDAD			: Listar los archivos del pase a producción
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_ARCHIVO_LISTAR_POR_CODIGO] 
	@CodPaseProduccion INT
AS
BEGIN   

	SELECT 
		A.CodArchivo,
		ISNULL(A.Nombre,'') AS Nombre,
		ISNULL(A.Origen,'') AS Origen,
		ISNULL(A.Destino,'') AS Destino,
		ISNULL(A.Modulo,0) AS Modulo
	FROM dbo.Archivo A
	WHERE A.CodPaseProduccion = @CodPaseProduccion

END
GO

DROP PROCEDURE IF EXISTS dbo.SP_SUSTENTO_LISTAR_POR_CODIGO
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 15/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: FERNANDO VASQUEZ
-- STORE PROCEDURE			: SP_SUSTENTO_LISTAR_POR_CODIGO
-- FUNCIONALIDAD			: Listar los sustentos del pase a producción
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_SUSTENTO_LISTAR_POR_CODIGO] 
	@CodPaseProduccion INT
AS
BEGIN   

	SELECT 
		S.CodSustento,
		ISNULL(S.Nombre,'') AS Nombre,
		S.CodLaserFiche
	FROM dbo.Sustento S WITH (NOLOCK)
	WHERE S.CodPaseProduccion = @CodPaseProduccion

END
GO

DROP PROCEDURE IF EXISTS [dbo].[SP_SGPP_ANALISTA_OBTENER]
GO

 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 17/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: FERNANDO VASQUEZ
-- STORE PROCEDURE			: [SP_SGPP_ANALISTA_OBTENER]
-- FUNCIONALIDAD			: Listar Analistas
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_SGPP_ANALISTA_OBTENER]
AS
BEGIN  
		SELECT
		CodUsuario,NombresApellidos
		FROM Usuario 
		WHERE upper(Perfil)='ANA'
		and Estado = 1
END
GO

DROP PROCEDURE IF EXISTS [dbo].[SP_USUARIO_LISTAR_CORREO_PORPERFIL]
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 17/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: FERNANDO VASQUEZ
-- STORE PROCEDURE			: SP_ESTADO_LISTAR
-- FUNCIONALIDAD			: Obtener la lista de correos por perfil
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_USUARIO_LISTAR_CORREO_PORPERFIL]
	@Perfil VARCHAR(5)
AS  
BEGIN
	DECLARE @Estado_Activo INT = 1 ;
	SELECT
		CorreoElectronico
	FROM Usuario
	WHERE Perfil = @Perfil 
	AND Estado = @Estado_Activo
END 
GO

DROP PROCEDURE IF EXISTS [dbo].[SP_USUARIO_OBTENER_CORREO_POR_CODPASEPRODUCCION]
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 17/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: FERNANDO VASQUEZ
-- STORE PROCEDURE			: SP_ESTADO_LISTAR
-- FUNCIONALIDAD			: Obtener el correo del usuario que registro el pase
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_USUARIO_OBTENER_CORREO_POR_CODPASEPRODUCCION]
	@CodPaseProduccion INT
AS  
BEGIN
	DECLARE @Estado_Activo INT = 1 ;
	SELECT
		US.CorreoElectronico
	FROM Usuario US
	INNER JOIN PaseProduccion PP ON PP.Analista = US.CodUsuario
	WHERE US.Estado = @Estado_Activo
	AND PP.CodPaseProduccion = @CodPaseProduccion
END 
GO

DROP PROCEDURE IF EXISTS [dbo].[SP_PASE_DATOS_FLUJO_OBTENER]
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 20/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: GERMAN MOLANO
-- STORE PROCEDURE			: SP_PASE_INSERTAR
-- FUNCIONALIDAD			: Inserta la información de un pase a producción
--================================================================================================================================
CREATE PROCEDURE SP_PASE_DATOS_FLUJO_OBTENER
(
	@CodPaseProduccion int
)
AS 
BEGIN
	
	 SELECT 
	 CASE b.Estado WHEN 1 THEN 'Formulario creado por '+c.NombresApellidos+' - '+format(b.FechaRegistro,'dd/MM/yyyy hh:mm:ss tt')
				   WHEN 2 THEN 'Formulario Aprobado por ejecutivo de sistemas '+c.NombresApellidos+' - '+format(b.FechaRegistro,'dd/MM/yyyy hh:mm:ss tt')
				   WHEN 3 THEN 'Formulario Aprobado por funcionario de calidad '+c.NombresApellidos+' - '+format(b.FechaRegistro,'dd/MM/yyyy hh:mm:ss tt')
				   WHEN 5 THEN 'Formulario Aprobado por soporte '+c.NombresApellidos+' - '+format(b.FechaRegistro,'dd/MM/yyyy hh:mm:ss tt')
				   WHEN 4 THEN 'Formulario devuelto por '+c.NombresApellidos+' - '+format(b.FechaRegistro,'dd/MM/yyyy hh:mm:ss tt')
				   WHEN 6 THEN 'Formulario anulado por '+c.NombresApellidos+' - '+format(b.FechaRegistro,'dd/MM/yyyy hh:mm:ss tt')
				   ELSE '' END DescripcionFujoPase
	 FROM PaseProduccion a
	 INNER JOIN DetalleEstado b ON a.CodPaseProduccion=b.CodPaseProduccion
	 INNER JOIN Usuario c ON c.CodUsuario=b.CodUsuario
	 INNER JOIN Parametro d ON d.Parametro=b.Estado AND Grupo=200
	 WHERE a.CodPaseProduccion=@CodPaseProduccion 
	 ORDER BY b.CodDetalleEstado ASC 
END 
GO

DROP PROCEDURE IF EXISTS [dbo].[SP_PASE_COMENTARIOS_FLUJO_OBTENER]
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 20/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: GERMAN MOLANO
-- STORE PROCEDURE			: SP_PASE_COMENTARIOS_FLUJO_OBTENER
-- FUNCIONALIDAD			: obtiene los datos del flujo
--================================================================================================================================
create procedure SP_PASE_COMENTARIOS_FLUJO_OBTENER
(
	@CodPaseProduccion int
)
AS 
BEGIN
	SELECT 
		a.CodPaseProduccion,
		c.NombresApellidos,
		b.FechaRegistro Fecha, 
		b.Comentario
	FROM PaseProduccion a
	INNER JOIN DetalleEstado b ON a.CodPaseProduccion=b.CodPaseProduccion
	INNER JOIN Usuario c ON c.CodUsuario=b.CodUsuario
	INNER JOIN Parametro d ON d.Parametro=b.Estado and Grupo=200
	WHERE a.CodPaseProduccion=@CodPaseProduccion
	ORDER BY b.CodDetalleEstado ASC 
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_PASE_ACTUALIZAR
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 18/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: FERNANDO VASQUEZ
-- STORE PROCEDURE			: SP_PASE_INSERTAR
-- FUNCIONALIDAD			: Inserta la información de un pase a producción
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_PASE_ACTUALIZAR] 
	@CodPaseProduccion     INT,
	@FechaPase             DATE,
	@Ticket                VARCHAR (100),
	@Descripcion           VARCHAR (1500),
	@UbicacionFuente       VARCHAR (500),
	@PaseInterno           BIT,
	@MotivoPase            INT,
	@Compilacion           INT,
	@CondicionesEspeciales VARCHAR (1500),
	@LibreriaFuente        INT,
	@LibreriaObjetoOtros   INT,
	@LibreriaObjetoPFyLF   INT,
	@Analista              INT,
	@UsuarioRegistro       VARCHAR(50),
	@Comentario            VARCHAR (1500),
	@TObjetos 		  	   TObjetos READONLY,
	@TArchivos	 		   TArchivos READONLY,
	@TSustentos 		   TSustentos READONLY,
	@Resultado			   BIT OUTPUT
AS
BEGIN   
	SET NOCOUNT ON

	DECLARE @Estado_DerEjecutivo INT = 1;
	DECLARE @Activo BIT = 1;
	DECLARE @Mensaje VARCHAR(MAX);

	UPDATE dbo.PaseProduccion
		SET FechaPase = @fechapase,
			Ticket = @ticket,
			Descripcion = @Descripcion,
			UbicacionFuente = @UbicacionFuente,
			PaseInterno = @PaseInterno,
			MotivoPase = @MotivoPase,
			Compilacion = @Compilacion,
			CondicionesEspeciales = @CondicionesEspeciales,
			LibreriaFuente = @libreriafuente,
			LibreriaObjetoOtros = @libreriaobjetootros,
			LibreriaObjetoPFyLF = @libreriaobjetopfylf,
			Estado = @Estado_DerEjecutivo,
			UsuarioModificacion = @usuarioregistro,
			FechaModificacion = GETDATE()
	WHERE CodPaseProduccion = @CodPaseProduccion

	/* Insertando el Detalle del Estado*/
	INSERT INTO dbo.DetalleEstado (
				CodPaseProduccion, 
				Estado, 
				Comentario, 
				UsuarioRegistro, 
				FechaRegistro, 
				CodUsuario
			)
	VALUES (
				@CodPaseProduccion, 
				@Estado_DerEjecutivo, 
				@comentario, 
				@usuarioregistro, 
				GETDATE(),
				@analista
			)
	/* Eliminando los Objetos Previos*/
	DELETE dbo.Objeto 
	WHERE CodPaseProduccion = @CodPaseProduccion

	/* Insertando Objetos */
	INSERT INTO dbo.Objeto (
				CodPaseProduccion, 
				Tipo, 
				Nombre, 
				Descripcion, 
				UsuarioRegistro,
				FechaRegistro
				)
	SELECT
				@CodPaseProduccion, 
				Tipo, 
				Nombre, 
				Descripcion, 
				@UsuarioRegistro, 
				GETDATE()
	FROM @TObjetos
	
	/* Eliminando los Objetos Previos*/
	DELETE dbo.Archivo 
	WHERE CodPaseProduccion = @CodPaseProduccion

	/* Insertando Archivos*/
	INSERT INTO dbo.Archivo (
				CodPaseProduccion, 
				Nombre, 
				Origen, 
				Destino, 
				Modulo, 
				UsuarioRegistro,
				FechaRegistro
				)
	SELECT
				@CodPaseProduccion, 
				Nombre, 
				Origen, 
				Destino, 
				Modulo, 
				@UsuarioRegistro,
				GETDATE()
	FROM @TArchivos

	/* Eliminando los Sustentos*/
	DELETE dbo.Sustento 
	WHERE CodPaseProduccion = @CodPaseProduccion

	SET @Resultado = 1
END
GO

DROP PROCEDURE IF EXISTS [dbo].[SP_OBSERVACION_OBTENER]
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 17/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: FERNANDO VASQUEZ
-- STORE PROCEDURE			: SP_ESTADO_LISTAR
-- FUNCIONALIDAD			: Obtener las observaciones de un pase a producción
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_OBSERVACION_OBTENER]
	@CodPaseProduccion VARCHAR(5)
AS  
BEGIN
  	SET NOCOUNT ON
	SELECT
		ISNULL(Compilacion,0) AS Compilacion,
		ISNULL(CondicionesEspeciales,'') AS CondicionesEspeciales
	FROM paseProduccion
	WHERE CodPaseProduccion = @CodPaseProduccion
END 
GO

DROP PROCEDURE IF EXISTS [dbo].[SP_INFORMACION_SOPORTE_OBTENER]
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 20/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: FERNANDO VASQUEZ
-- STORE PROCEDURE			: SP_INFORMACION_SOPORTE_OBTENER
-- FUNCIONALIDAD			: Obtener las informacion de soporte segun el tipo de pase a producción
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_INFORMACION_SOPORTE_OBTENER]
	@TipoPase INT
AS  
BEGIN
  	SET NOCOUNT ON

	DECLARE @Estado_Activo INT = 1;
	DECLARE @Orden_Titulo INT = 1;
	DECLARE @Orden_Item INT = 2;
	DECLARE @Grupo_Informacion_Soporte INT = 500;

	SELECT 
		T.Valor AS Titulo,
		IT.Valor AS Item
	FROM Parametro IT 
	INNER JOIN Parametro T ON IT.Referencia = T.Referencia
	WHERE IT.GRUPO = @Grupo_Informacion_Soporte
	AND IT.Parametro = @TipoPase
	AND IT.Orden = @Orden_Item
	AND IT.Estado = @Estado_Activo
	AND T.GRUPO = @Grupo_Informacion_Soporte
	AND T.Parametro = @TipoPase
	AND T.Orden = @Orden_Titulo
	AND T.Estado = @Estado_Activo
END 
GO

DROP PROCEDURE IF EXISTS [dbo].[SP_PASE_ACTUALIZAR_ESTADO]
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 21/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: FERNANDO VASQUEZ
-- STORE PROCEDURE			: SP_PASE_ACTUALIZAR_ESTADO
-- FUNCIONALIDAD			: Actualiza el estado del pase a producción
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_PASE_ACTUALIZAR_ESTADO] 
	@CodPase			   INT,
	@Comentario            VARCHAR (1500),
	@UsuarioRegistro       VARCHAR(50),
	@Perfil				   VARCHAR(10),
	@Estado				   INT,
	@ActualizacionOk	   INT OUTPUT
AS
BEGIN   
	SET NOCOUNT ON

	DECLARE @Mensaje VARCHAR(MAX);
	SET @ActualizacionOk = 0
	/* Obteniendo el CodUsuario*/
	DECLARE @CodUsuario INT;
	SET @CodUsuario = (SELECT CodUsuario FROM Usuario WHERE UsuarioWeb = @UsuarioRegistro AND Perfil=UPPER(@Perfil))

	BEGIN TRY
		BEGIN TRAN
			/* Insertando el Detalle del Estado*/
			INSERT INTO dbo.DetalleEstado (
						CodPaseProduccion, 
						Estado, 
						Comentario, 
						UsuarioRegistro, 
						FechaRegistro, 
						CodUsuario
					)
			VALUES (
						@CodPase, 
						@Estado, 
						@Comentario, 
						@UsuarioRegistro, 
						GETDATE(),
						@CodUsuario
					)

			UPDATE dbo.PaseProduccion
			SET Estado = @Estado,
				UsuarioModificacion = @UsuarioRegistro,
				FechaModificacion = GETDATE()
			WHERE CodPaseProduccion = @CodPase
		COMMIT TRAN

		SET @ActualizacionOk = 1

	END TRY
	BEGIN CATCH 
		ROLLBACK 
		SET @Mensaje = ERROR_MESSAGE()
		SET @ActualizacionOk = 0
		RAISERROR(@Mensaje,16,-1)
	END CATCH
	
END
GO

DROP PROCEDURE IF EXISTS [dbo].[SP_SGPP_LIBRERIA_FUENTE_OBTENER]
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 21/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: FERNANDO VASQUEZ
-- STORE PROCEDURE			: SP_SGPP_LIBRERIA_FUENTE_OBTENER
-- FUNCIONALIDAD			: Obtiene la informacion de la libreria de fuente
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_SGPP_LIBRERIA_FUENTE_OBTENER]
AS
BEGIN  
	
	DECLARE @Grupo_Libreria_Fuente INT = 700;
	DECLARE @Estado_Activo INT = 1
	SELECT
		P.Descripcion AS Descripcion,
		P.Parametro AS Valor
	FROM PARAMETRO AS P
	WHERE P.Grupo = @Grupo_Libreria_Fuente
	AND P.Estado = @Estado_Activo
END

GO

DROP PROCEDURE IF EXISTS [dbo].[SP_SGPP_LIBRERIA_OBJETO_OBTENER]
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 21/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: FERNANDO VASQUEZ
-- STORE PROCEDURE			: SP_SGPP_LIBRERIA_OBJETO_OBTENER
-- FUNCIONALIDAD			: Obtiene la informacion de la libreria de objeto
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_SGPP_LIBRERIA_OBJETO_OBTENER]
AS
BEGIN  

	DECLARE @Grupo_Libreria_Objeto INT = 800;
	DECLARE @Estado_Activo INT = 1
	SELECT
		P.Descripcion AS Descripcion,
		P.Parametro AS Valor
	FROM PARAMETRO AS P
	WHERE P.Grupo = @Grupo_Libreria_Objeto
	AND P.Estado = @Estado_Activo

END

GO

DROP PROCEDURE IF EXISTS [dbo].[SP_SGPP_LIBRERIA_OBJETO_PFLF_OBTENER]
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 21/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: FERNANDO VASQUEZ
-- STORE PROCEDURE			: SP_SGPP_LIBRERIA_OBJETO_PFLF_OBTENER
-- FUNCIONALIDAD			: Obtiene la informacion de la libreria de objeto PFLF
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_SGPP_LIBRERIA_OBJETO_PFLF_OBTENER]
AS
BEGIN  

	DECLARE @Grupo_Libreria_Objeto_PFLF INT = 900;
	DECLARE @Estado_Activo INT = 1
	SELECT
		P.Descripcion AS Descripcion,
		P.Parametro AS Valor
	FROM PARAMETRO AS P
	WHERE P.Grupo = @Grupo_Libreria_Objeto_PFLF
	AND P.Estado = @Estado_Activo

END

GO


DROP PROCEDURE IF EXISTS [dbo].[SP_SGPP_MODULO_PFLF_OBTENER]
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 22/02/2022
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: FERNANDO VASQUEZ
-- STORE PROCEDURE			: SP_SGPP_MODULO_PFLF_OBTENER
-- FUNCIONALIDAD			: Obtiene la informacion de modulo PFLF
--================================================================================================================================
CREATE PROCEDURE [dbo].[SP_SGPP_MODULO_PFLF_OBTENER]
AS
BEGIN  

	DECLARE @Grupo_Modulo_PFLF INT = 1000;
	DECLARE @Estado_Activo INT = 1
	SELECT
		P.Descripcion AS Descripcion,
		P.Parametro AS Valor
	FROM PARAMETRO AS P
	WHERE P.Grupo = @Grupo_Modulo_PFLF
	AND P.Estado = @Estado_Activo

END

GO