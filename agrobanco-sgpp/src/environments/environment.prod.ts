export const environment = {
  production: true,
  UrlBase_SSA: (window as any)["env"]["UrlBase_SSA"],
  UrlBase_SGPPAPI:  (window as any)["env"]["UrlBase_SGPPAPI"],
  AppKey:  (window as any)["env"]["AppKey"],
  AppCode :  (window as any)["env"]["AppCode"]
};
