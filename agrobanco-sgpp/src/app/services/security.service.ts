import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { forkJoin, Observable, of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { Usuario } from '../components/models/usuario.interface';
import { DataSeguridad } from '../components/models/dataseguridad.interface';
import { Modulo } from '../components/models/modulo.interface';

const UrlBase_SGSAPI = environment.UrlBase_SGPPAPI+"/sgs";
const UrlBase_SSA = environment.UrlBase_SSA;
const UrlBase_SGPPAPI = environment.UrlBase_SGPPAPI;
const APP_CODE = environment.AppCode;
const APP_KEY = environment.AppKey;

@Injectable({
  providedIn: 'root'
})
export class SecurityService {
  //private TokenSGS! : string;
  public activarLoading : boolean = false;
  public modulos: Modulo[] = [];
  constructor() { }

  

  public guardarToken( token: any ): void {
    localStorage.setItem('tokenSGS', token.toString());
  }

  public guardaridPerfil( idPerfil: string | any ): void {
    localStorage.setItem('idperfil', idPerfil.toString());
  }

  public guardarPerfil(perfil: string | any): void {
    localStorage.setItem('perfil', perfil.toString());
  }

  public guardarDataSeguridad(data : DataSeguridad)
  {
    localStorage.setItem('vEmail', data.vEmail.toString());
    localStorage.setItem('vNombre',  data.vNombre.toString());
    localStorage.setItem('UsuarioWeb',  data.vUsuarioWeb.toString());    
  }

  
  public leerEmail(): any {
    
    let email: any;

    if ( localStorage.getItem('vEmail') ) {
      email = localStorage.getItem('vEmail');
    } else {
      email = '';
    }

    return email;
  }

  public leerTokenSeguridad(): any {

    let Token: any;

    if ( localStorage.getItem('tokenSGS') ) {
      Token = localStorage.getItem('tokenSGS');
    } else {
      Token = '';
    }

    return Token;
  }

  public leeridPerfil(): any {

    let idperfil: any;

    if ( localStorage.getItem('idperfil') ) {
      idperfil = localStorage.getItem('idperfil');
    } else {
      idperfil = '';
    }

    return idperfil;
  }

  public leerUsuarioWeb(): any {

    let usuarioWeb: any;

    if ( localStorage.getItem('UsuarioWeb') ) {
      usuarioWeb = localStorage.getItem('UsuarioWeb');
    } else {
      usuarioWeb = '';
    }

    return usuarioWeb;
  }

  public leerNombreUsuario(): any {

    let nombreUsuario: any;

    if ( localStorage.getItem('vNombre') ) {
      nombreUsuario = localStorage.getItem('vNombre');
    } else {
      nombreUsuario = '';
    }

    return nombreUsuario;
  }

  public validarToken(): Observable<any>{
    let tokenSGS = this.leerTokenSeguridad();
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${ tokenSGS }`
    }

    const req = ajax.get(`${UrlBase_SGSAPI}/validate/token`, headers);

    const data$ = new Observable(observer=> {
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;
  }

  public obtenerModulos(): Observable<any>{
    let tokenSGS = this.leerTokenSeguridad();
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${ tokenSGS }`
    }

    let idPerfil = this.leeridPerfil();
    let usuarioWeb = this.leerUsuarioWeb();
    const req = ajax.get(`${UrlBase_SGSAPI}/modulo/obtenermodulos/${idPerfil}/${usuarioWeb}`, headers);

    const data$ = new Observable(observer=> {
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;
  }
  
  // public guardarModulos(data : Modulo[]) {
  //   localStorage.setItem('modulos', JSON.stringify(data));
  // }

  public leerModulos() {
    let modulos: any;
    var item = localStorage.getItem('modulos');
    if ( item != null ) {      
        modulos = JSON.parse(item);
    } else {
      modulos = null;
    }

    return modulos;
  }

  public tienePermisoModulo():boolean{
    return true;
  }

  public redirectLogin(): void {
    window.location.href = `${UrlBase_SSA}`;
  }
  
  public obtenerUsuario (filtro : Usuario): Observable<any> {
    
    let tokenSGS = this.leerTokenSeguridad();
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${ tokenSGS }`
    }

    const req = ajax.post(`${UrlBase_SGSAPI}/usuario/obtenerusuario`, filtro, headers);

    const data$ = new Observable(observer=> {
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;
  }

  
  public autenticarSAR(): Observable<any> {
    
    let email = this.leerEmail();
    let usuarioWeb = this.leerUsuarioWeb();
    let nombreUsuario = this.leerNombreUsuario();

    let filtros =  {
      correoelectronico : email,
      nombreusuario : nombreUsuario,
      codigousuario : usuarioWeb,
      numerodocumento : usuarioWeb
    };

    const headers = {
      'Content-Type': 'application/json',
      'X-AppKey' : `${ APP_KEY }`,
      'X-AppCode' : `${ APP_CODE }`
    }

    const req = ajax.post(`${UrlBase_SGPPAPI}/acceso`, filtros, headers);

    const data$ = new Observable(observer=> {
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;
  }

  guardarTokenSAR(tokenSAR: string){
    localStorage.setItem('tokenSAR', tokenSAR);
  }

  public leerTokenSGPP(){
    let tokenSAR: any;

    if ( localStorage.getItem('tokenSAR') ) {
      tokenSAR = localStorage.getItem('tokenSAR');
    } else {
      tokenSAR = '';
    }

    return tokenSAR;
  }
  

}
