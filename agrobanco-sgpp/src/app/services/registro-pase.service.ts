import { Injectable, Type } from '@angular/core';
import { SecurityService } from './security.service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AppService } from '../app.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RegPaseRequest } from '../components/models/reg-pase-request.interface';
import { PaseWorkflow } from '../components/models/pase-workflow.interface';

@Injectable({
  providedIn: 'root'
})
export class RegistroPaseService extends AppService {

  constructor(
    _securityService : SecurityService,
    private httpClient: HttpClient
  ) {
    super(_securityService);
   }

  ObtenerPaseLista<T>(datos:any):Observable<any>{
    this.endpoint =  `${environment.UrlBase_SGPPAPI}/registro-aprobacion-pase/listar`;
    return this.post(this.endpoint,datos);
  }

  ObtenerEstadoLista<T>():Observable<any>{
    this.endpoint =  `${environment.UrlBase_SGPPAPI}/registro-aprobacion-pase/listarEstadosPase`;
    return this.post(this.endpoint);
  }

  ObtenerTipoPlataformaLista<T>():Observable<any>{
    this.endpoint =  `${environment.UrlBase_SGPPAPI}/registro-aprobacion-pase/listarTipoPlataforma`;
    return this.post(this.endpoint);
  }

  ObtenerListaLibrerias(Tipo: number):Observable<any>{ 
    this.endpoint =  `${environment.UrlBase_SGPPAPI}/registro-aprobacion-pase/Librerias/${Tipo}`;
    return this.get(this.endpoint);
  }

  ObtenerListaAnalistas():Observable<any>{ 
    this.endpoint =  `${environment.UrlBase_SGPPAPI}/registro-aprobacion-pase/Analistas`;
    return this.get(this.endpoint);
  }

  ObtenerPasePorCodigo(codPase: number):Observable<any>{
    this.endpoint =  `${environment.UrlBase_SGPPAPI}/registro-aprobacion-pase/obtenerPase/${codPase}`;
    return this.get(this.endpoint);
  }


  ObtenerDatosFlujo(codPase: number):Observable<any>{
    this.endpoint =  `${environment.UrlBase_SGPPAPI}/registro-aprobacion-pase/obtenerDatosFlujo/${codPase}`;
    return this.get(this.endpoint);
  }

  ObtenerComentariosFlujo(codPase: number):Observable<any>{
    this.endpoint =  `${environment.UrlBase_SGPPAPI}/registro-aprobacion-pase/ObtenerComentariosFlujo/${codPase}`;
    return this.get(this.endpoint);
  }

  ObtenerListaArchivos(codPase: number):Observable<any>{
    this.endpoint =  `${environment.UrlBase_SGPPAPI}/registro-aprobacion-pase/archivos/${codPase}`;
    return this.get(this.endpoint);
  }

  ObtenerListaObjetos(codPase: number):Observable<any>{
    this.endpoint =  `${environment.UrlBase_SGPPAPI}/registro-aprobacion-pase/objetos/${codPase}`;
    return this.get(this.endpoint);
  }

  ObtenerInfoSoporte(tipoPase: number):Observable<any>{ 
    this.endpoint =  `${environment.UrlBase_SGPPAPI}/registro-aprobacion-pase/ObtenerInformacionSoporte/${tipoPase}`;
    return this.get(this.endpoint);
  }

  ObtenerObservaciones(codPase: number):Observable<any>{
    this.endpoint =  `${environment.UrlBase_SGPPAPI}/registro-aprobacion-pase/observaciones/${codPase}`;
    return this.get(this.endpoint);
  }

  ObtenerListaSustentos(codPase: number):Observable<any>{
    this.endpoint =  `${environment.UrlBase_SGPPAPI}/registro-aprobacion-pase/sustentos/${codPase}`;
    return this.get(this.endpoint);
  }

  DescargarSustento(idLaserfiche: number):Observable<any>{
    this.endpoint =  `${environment.UrlBase_SGPPAPI}/registro-aprobacion-pase/descargar/${idLaserfiche}`;
    
    const token = this.securityService.leerTokenSGPP();

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Token': `${ token }`
    });

    return this.httpClient.get(this.endpoint, { headers, responseType: 'blob' });
  }

  InsertarPase(request: RegPaseRequest):Observable<any>{
    this.endpoint =  `${environment.UrlBase_SGPPAPI}/registro-aprobacion-pase/RegistrarPase`;
    return this.post(this.endpoint, request);
  }

  ActualizarPase(request: RegPaseRequest):Observable<any>{
    this.endpoint =  `${environment.UrlBase_SGPPAPI}/registro-aprobacion-pase/ActualizarPase`;
    return this.post(this.endpoint, request);
  }

  AprobarPase(paseWorkflow: PaseWorkflow):Observable<any>{
    this.endpoint =  `${environment.UrlBase_SGPPAPI}/registro-aprobacion-pase/AprobarPase`;
    return this.post(this.endpoint, paseWorkflow);
  }

  RechazarPase(paseWorkflow: PaseWorkflow):Observable<any>{
    this.endpoint =  `${environment.UrlBase_SGPPAPI}/registro-aprobacion-pase/RechazarPase`;
    return this.post(this.endpoint, paseWorkflow);
  }

  AnularPase(paseWorkflow: PaseWorkflow):Observable<any>{
    this.endpoint =  `${environment.UrlBase_SGPPAPI}/registro-aprobacion-pase/AnularPase`;
    return this.post(this.endpoint, paseWorkflow);
  }

}
