import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { Modulo } from '../components/models/modulo.interface';
import { DomService } from '../services/dom.service';
import { SecurityService } from '../services/security.service';

@Injectable()
export class AuthGuard implements CanActivate {

    public loader = false;
    constructor(private securityService: SecurityService, private router : Router, private domService : DomService) { }
    
    canActivate() : boolean
    {
        const token = this.securityService.leerTokenSeguridad();        
        this.loader = true;

        if (token === '' || token == null) {
            this.securityService.redirectLogin();
            return false;
        }


        this.securityService.validarToken().subscribe(
           (resp:any)=>{              

               this.securityService.obtenerModulos().subscribe(
                   (result: any) => {
                       
                       //Se valida si entre los modulos que se obtiene, se cuenta con el modulo al cual se quiere acceder
                       //var modulos = result.response as Modulo[];
                       let snapshot = this.router.routerState.snapshot;
                       let rutaControllerSolicitada = snapshot.url.split('/')[1];
                       this.securityService.modulos = result.response as Modulo[];
                    //    if()
                    //    {

                    //    }
                       
                    //    let rutaActionSolicitada = snapshot.url.split('/')[2];
                       
                       //console.log('ruta pedida: ' + rutaControllerSolicitada);

                       if (rutaControllerSolicitada != 'home' &&
                           rutaControllerSolicitada != 'detalle-registro-pase' &&
                           rutaControllerSolicitada != 'crear-registro-pase'
                            ) {
                        var found = false;
                        
                        for (let i = 0; i < this.securityService.modulos.length; i++) {                            
                            const element = this.securityService.modulos[i];
                            let acceso = '';

                            if(element.tipoOpcion == '1' || element.tipoOpcion == '2')
                            {
                                acceso = element.urlOpcion;
                            }

                            if(element.tipoOpcion == '3')
                            {
                                acceso = element.controller;
                            }

                            if (acceso.toUpperCase() == rutaControllerSolicitada.toUpperCase()) {
                                found = true;
                                break;
                            }
                        }

                        if (!found) {         
                            this.router.navigate(['error']);                   
                            this.loader = false;
                            return false;
                        }
                       }
                       
                       //this.securityService.modulos = modulos;                       
                       
                       if(this.securityService.modulos.length > 0)
                       {
                           this.securityService.guardarPerfil(this.securityService.modulos[0].nombrePerfil);
                       }
                       
                       this.loader = false;
                       this.domService.HideSideBar();
                       return true;
                   },
                   (error) => {  
                    console.log(error);
                    this.router.navigate(['error']);
                    this.loader = false;
                    return false;
                   });

               return true;
           },
           (error) => {
            console.log(error);
            this.securityService.redirectLogin();
            return false;
          }
        );

        return true;
    }    
}