
export interface RegPaseRequest{

    CodPaseProduccion: number,
    TipoPase: number;
    Ticket: string;
    Descripcion: string;
    UbicacionFuente: string;
    PaseInterno: number;
    MotivoPase: number;
    Compilacion: number;
    CondicionesEspeciales: string;
    LibreriaFuente: number;
    LibreriaObjetoOtros: number;
    LibreriaObjetoPFyLF: number;
    Analista: number;
    UsuarioRegistro: string;
    Comentario: string;
    Objetos: Objeto[];
    Archivos: Archivo[];
    Sustentos: Sustento[];
}

export interface Objeto{
    Tipo: number;
    Nombre: string;
    Descripcion: string;
}

export interface Archivo{
    Nombre: string;
    Origen: string;
    Destino: string;
    Modulo: number;
}

export interface Sustento{
    Nombre: string;
    Archivo: string;
    CodLaserFiche: number;
}