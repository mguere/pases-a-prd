
export interface PaseWorkflow{
    CodPase: number;
    Perfil: string;
    Comentario: string;
    UsuarioRegistro: string;
}