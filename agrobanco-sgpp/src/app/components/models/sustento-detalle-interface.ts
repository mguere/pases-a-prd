
export interface SustentoDetalle{
    codLaserFiche: number;
    nombre: string;
    nombreAbreviatura: string;
}