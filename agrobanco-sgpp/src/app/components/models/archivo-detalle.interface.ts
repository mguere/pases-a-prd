export interface ArchivoDetalle{
    codArchivo: number;
    nombre: string;
    origen: string;
    destino: string;
    modulo: number;
}