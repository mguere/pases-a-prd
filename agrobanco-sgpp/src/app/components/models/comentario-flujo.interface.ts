export interface comentarioFlujo{
    CodPaseProduccion: number;
    NombresApellidos: string;
    Fecha: Date;
    Comentario: string;
}