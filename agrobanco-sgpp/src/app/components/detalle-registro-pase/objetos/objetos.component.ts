import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Estados, Perfiles, TipoObjeto } from 'src/app/helpers/constantes';
import { RegistroPaseService } from 'src/app/services/registro-pase.service';
import { ObjetoDetalle } from '../../models/objeto-detalle.interface';
import { Pase } from '../../models/pase.interface';

@Component({
    selector: 'app-objetos',
    templateUrl: './objetos.component.html',
    styleUrls: ['./objetos.component.css']
})
export class ObjetosComponent implements OnInit, OnChanges, OnDestroy  {

  loading: boolean = false;
  objetos! : FormGroup;
  @Input() pase!: Pase;

  lstObjetos: ObjetoDetalle[] = [];
  objetosSubs!: Subscription;

  eANA: Perfiles = Perfiles.ANA;
  cPerfil!: string;

  constructor(
    private formBuilder: FormBuilder,
    private regPaseService: RegistroPaseService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.pase){
      this.cargarObjetos();
    }
  }

  ngOnInit(): void {
    this.cPerfil = localStorage.getItem('perfil')!;

    this.objetos = this.formBuilder.group({
      objeto1_nombre: [''],
      objeto1_descripcion: [''],
      objeto2_nombre: [''],
      objeto2_descripcion: [''],
      objeto3_nombre: [''],
      objeto3_descripcion: [''],

      objeto4_nombre: [''],
      objeto4_descripcion: [''],
      objeto5_nombre: [''],
      objeto5_descripcion: [''],
      objeto6_nombre: [''],
      objeto6_descripcion: [''],

      objeto7_nombre: [''],
      objeto7_descripcion: [''],
      objeto8_nombre: [''],
      objeto8_descripcion: [''],
      objeto9_nombre: [''],
      objeto9_descripcion: [''],

      objeto10_nombre: [''],
      objeto10_descripcion: [''],
      objeto11_nombre: [''],
      objeto11_descripcion: [''],
      objeto12_nombre: [''],
      objeto12_descripcion: [''],

      objeto13_nombre: [''],
      objeto13_descripcion: [''],
      objeto14_nombre: [''],
      objeto14_descripcion: [''],
      objeto15_nombre: [''],
      objeto15_descripcion: [''],

      objeto16_nombre: [''],
      objeto16_descripcion: [''],
      objeto17_nombre: [''],
      objeto17_descripcion: [''],
      objeto18_nombre: [''],
      objeto18_descripcion: [''],

      objeto19_nombre: [''],
      objeto19_descripcion: [''],
      objeto20_nombre: [''],
      objeto20_descripcion: [''],
      objeto21_nombre: [''],
      objeto21_descripcion: [''],

      objeto22_nombre: [''],
      objeto22_descripcion: [''],
      objeto23_nombre: [''],
      objeto23_descripcion: [''],
      objeto24_nombre: [''],
      objeto24_descripcion: [''],

      objeto25_nombre: [''],
      objeto25_descripcion: [''],
      objeto26_nombre: [''],
      objeto26_descripcion: [''],
      objeto27_nombre: [''],
      objeto27_descripcion: [''],

      objeto28_nombre: [''],
      objeto28_descripcion: [''],
      objeto29_nombre: [''],
      objeto29_descripcion: [''],
      objeto30_nombre: [''],
      objeto30_descripcion: ['']
    });
  }

  cargarObjetos(){
    this.loading = true;
    this.objetosSubs = this.regPaseService.ObtenerListaObjetos(this.pase.codPaseProduccion)
      .subscribe(
        data => {
          this.lstObjetos = data.response as ObjetoDetalle[];
          this.setObjetos();
        },
        err => {
          console.log(err);
        },
        () => {
          this.loading = false;
        }
      )
  }

  get f(){
    return this.objetos.controls;
  }

  private setObjetos(){

    if(!this.lstObjetos) return;

    let lstType1 = this.lstObjetos.filter(item => item.tipo === TipoObjeto.QDDSSRC);
    
    if(lstType1){
      for(let i = 0; i< lstType1.length; i++){
        if(i == 0){
          this.f.objeto1_nombre.setValue(lstType1[i].nombre);
          this.f.objeto1_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 1){
          this.f.objeto4_nombre.setValue(lstType1[i].nombre);
          this.f.objeto4_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 2){
          this.f.objeto7_nombre.setValue(lstType1[i].nombre);
          this.f.objeto7_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 3){
          this.f.objeto10_nombre.setValue(lstType1[i].nombre);
          this.f.objeto10_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 4){
          this.f.objeto13_nombre.setValue(lstType1[i].nombre);
          this.f.objeto13_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 5){
          this.f.objeto16_nombre.setValue(lstType1[i].nombre);
          this.f.objeto16_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 6){
          this.f.objeto19_nombre.setValue(lstType1[i].nombre);
          this.f.objeto19_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 7){
          this.f.objeto22_nombre.setValue(lstType1[i].nombre);
          this.f.objeto22_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 8){
          this.f.objeto25_nombre.setValue(lstType1[i].nombre);
          this.f.objeto25_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 9){
          this.f.objeto28_nombre.setValue(lstType1[i].nombre);
          this.f.objeto28_descripcion.setValue(lstType1[i].descripcion);
        }
      }
    }

    lstType1 = this.lstObjetos.filter(item => item.tipo === TipoObjeto.QCLSRC);

    if(lstType1){
      for(let i = 0; i< lstType1.length; i++){
        if(i == 0){
          this.f.objeto2_nombre.setValue(lstType1[i].nombre);
          this.f.objeto2_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 1){
          this.f.objeto5_nombre.setValue(lstType1[i].nombre);
          this.f.objeto5_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 2){
          this.f.objeto8_nombre.setValue(lstType1[i].nombre);
          this.f.objeto8_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 3){
          this.f.objeto11_nombre.setValue(lstType1[i].nombre);
          this.f.objeto11_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 4){
          this.f.objeto14_nombre.setValue(lstType1[i].nombre);
          this.f.objeto14_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 5){
          this.f.objeto17_nombre.setValue(lstType1[i].nombre);
          this.f.objeto17_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 6){
          this.f.objeto20_nombre.setValue(lstType1[i].nombre);
          this.f.objeto20_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 7){
          this.f.objeto23_nombre.setValue(lstType1[i].nombre);
          this.f.objeto23_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 8){
          this.f.objeto26_nombre.setValue(lstType1[i].nombre);
          this.f.objeto26_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 9){
          this.f.objeto29_nombre.setValue(lstType1[i].nombre);
          this.f.objeto29_descripcion.setValue(lstType1[i].descripcion);
        }
      }
    }

    lstType1 = this.lstObjetos.filter(item => item.tipo === TipoObjeto.QRPGSRC);

    if(lstType1){
      for(let i = 0; i< lstType1.length; i++){
        if(i == 0){
          this.f.objeto3_nombre.setValue(lstType1[i].nombre);
          this.f.objeto3_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 1){
          this.f.objeto6_nombre.setValue(lstType1[i].nombre);
          this.f.objeto6_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 2){
          this.f.objeto9_nombre.setValue(lstType1[i].nombre);
          this.f.objeto9_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 3){
          this.f.objeto12_nombre.setValue(lstType1[i].nombre);
          this.f.objeto12_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 4){
          this.f.objeto15_nombre.setValue(lstType1[i].nombre);
          this.f.objeto15_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 5){
          this.f.objeto18_nombre.setValue(lstType1[i].nombre);
          this.f.objeto18_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 6){
          this.f.objeto21_nombre.setValue(lstType1[i].nombre);
          this.f.objeto21_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 7){
          this.f.objeto24_nombre.setValue(lstType1[i].nombre);
          this.f.objeto24_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 8){
          this.f.objeto27_nombre.setValue(lstType1[i].nombre);
          this.f.objeto27_descripcion.setValue(lstType1[i].descripcion);
        }
        if(i == 9){
          this.f.objeto30_nombre.setValue(lstType1[i].nombre);
          this.f.objeto30_descripcion.setValue(lstType1[i].descripcion);
        }
      }
    }

    if(this.cPerfil !== this.eANA || this.pase.estado !== Estados.DevAnalista){
      this.f.objeto1_nombre.disable();
      this.f.objeto1_descripcion.disable();
      this.f.objeto2_nombre.disable();
      this.f.objeto2_descripcion.disable();
      this.f.objeto3_nombre.disable();
      this.f.objeto3_descripcion.disable();
      this.f.objeto4_nombre.disable();
      this.f.objeto4_descripcion.disable();
      this.f.objeto5_nombre.disable();
      this.f.objeto5_descripcion.disable();
      this.f.objeto6_nombre.disable();
      this.f.objeto6_descripcion.disable();
      this.f.objeto7_nombre.disable();
      this.f.objeto7_descripcion.disable();
      this.f.objeto8_nombre.disable();
      this.f.objeto8_descripcion.disable();
      this.f.objeto9_nombre.disable();
      this.f.objeto9_descripcion.disable();
      this.f.objeto10_nombre.disable();
      this.f.objeto10_descripcion.disable();
      this.f.objeto11_nombre.disable();
      this.f.objeto11_descripcion.disable();
      this.f.objeto12_nombre.disable();
      this.f.objeto12_descripcion.disable();
      this.f.objeto13_nombre.disable();
      this.f.objeto13_descripcion.disable();
      this.f.objeto14_nombre.disable();
      this.f.objeto14_descripcion.disable();
      this.f.objeto15_nombre.disable();
      this.f.objeto15_descripcion.disable();
      this.f.objeto16_nombre.disable();
      this.f.objeto16_descripcion.disable();
      this.f.objeto17_nombre.disable();
      this.f.objeto17_descripcion.disable();
      this.f.objeto18_nombre.disable();
      this.f.objeto18_descripcion.disable();
      this.f.objeto19_nombre.disable();
      this.f.objeto19_descripcion.disable();
      this.f.objeto20_nombre.disable();
      this.f.objeto20_descripcion.disable();
      this.f.objeto21_nombre.disable();
      this.f.objeto21_descripcion.disable();
      this.f.objeto22_nombre.disable();
      this.f.objeto22_descripcion.disable();
      this.f.objeto23_nombre.disable();
      this.f.objeto23_descripcion.disable();
      this.f.objeto24_nombre.disable();
      this.f.objeto24_descripcion.disable();
      this.f.objeto25_nombre.disable();
      this.f.objeto25_descripcion.disable();
      this.f.objeto26_nombre.disable();
      this.f.objeto26_descripcion.disable();
      this.f.objeto27_nombre.disable();
      this.f.objeto27_descripcion.disable();
      this.f.objeto28_nombre.disable();
      this.f.objeto28_descripcion.disable();
      this.f.objeto29_nombre.disable();
      this.f.objeto29_descripcion.disable();
      this.f.objeto30_nombre.disable();
      this.f.objeto30_descripcion.disable();
    }

  }

  ngOnDestroy(): void {
    this.objetosSubs?.unsubscribe();
  }

}