import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { DialogService } from 'src/app/services/comun/dialog.service';

@Component({
    selector: 'app-sustento',
    templateUrl: './sustento.component.html',
    styleUrls: ['./sustento.component.css']
})
export class SustentoComponent implements OnInit  {

  MAXIMO_TAMANIO_BYTES: number = 12582912;
  
  @ViewChild('file1', { static: false }) file1!: ElementRef;
  @ViewChild('file1Placeholder') file1Placeholder!: ElementRef;
  @ViewChild('file2', { static: false }) file2!: ElementRef;
  @ViewChild('file2Placeholder') file2Placeholder!: ElementRef;
  @ViewChild('file3', { static: false }) file3!: ElementRef;
  @ViewChild('file3Placeholder') file3Placeholder!: ElementRef;
  @ViewChild('file4', { static: false }) file4!: ElementRef;
  @ViewChild('file4Placeholder') file4Placeholder!: ElementRef;
  @ViewChild('file5', { static: false }) file5!: ElementRef;
  @ViewChild('file5Placeholder') file5Placeholder!: ElementRef;
  @ViewChild('file6', { static: false }) file6!: ElementRef;
  @ViewChild('file6Placeholder') file6Placeholder!: ElementRef;
  @ViewChild('file7', { static: false }) file7!: ElementRef;
  @ViewChild('file7Placeholder') file7Placeholder!: ElementRef;
  @ViewChild('file8', { static: false }) file8!: ElementRef;
  @ViewChild('file8Placeholder') file8Placeholder!: ElementRef;
  @ViewChild('file9', { static: false }) file9!: ElementRef;
  @ViewChild('file9Placeholder') file9Placeholder!: ElementRef;
  @ViewChild('file10', { static: false }) file10!: ElementRef;
  @ViewChild('file10Placeholder') file10Placeholder!: ElementRef;
  @ViewChild('file11', { static: false }) file11!: ElementRef;
  @ViewChild('file11Placeholder') file11Placeholder!: ElementRef;
  @ViewChild('file12', { static: false }) file12!: ElementRef;
  @ViewChild('file12Placeholder') file12Placeholder!: ElementRef;
  @ViewChild('file13', { static: false }) file13!: ElementRef;
  @ViewChild('file13Placeholder') file13Placeholder!: ElementRef;
  @ViewChild('file14', { static: false }) file14!: ElementRef;
  @ViewChild('file14Placeholder') file14Placeholder!: ElementRef;
  @ViewChild('file15', { static: false }) file15!: ElementRef;
  @ViewChild('file15Placeholder') file15Placeholder!: ElementRef;
  @ViewChild('file16', { static: false }) file16!: ElementRef;
  @ViewChild('file16Placeholder') file16Placeholder!: ElementRef;
  @ViewChild('file17', { static: false }) file17!: ElementRef;
  @ViewChild('file17Placeholder') file17Placeholder!: ElementRef;
  @ViewChild('file18', { static: false }) file18!: ElementRef;
  @ViewChild('file18Placeholder') file18Placeholder!: ElementRef;
  @ViewChild('file19', { static: false }) file19!: ElementRef;
  @ViewChild('file19Placeholder') file19Placeholder!: ElementRef;
  @ViewChild('file20', { static: false }) file20!: ElementRef;
  @ViewChild('file20Placeholder') file20Placeholder!: ElementRef;

  

  file1Name: string = '';
  archivoFile1: any;
  file2Name: string = '';
  archivoFile2: any;
  file3Name: string = '';
  archivoFile3: any;
  file4Name: string = '';
  archivoFile4: any;
  file5Name: string = '';
  archivoFile5: any;
  file6Name: string = '';
  archivoFile6: any;
  file7Name: string = '';
  archivoFile7: any;
  file8Name: string = '';
  archivoFile8: any;
  file9Name: string = '';
  archivoFile9: any;
  file10Name: string = '';
  archivoFile10: any;
  file11Name: string = '';
  archivoFile11: any;
  file12Name: string = '';
  archivoFile12: any;
  file13Name: string = '';
  archivoFile13: any;
  file14Name: string = '';
  archivoFile14: any;
  file15Name: string = '';
  archivoFile15: any;
  file16Name: string = '';
  archivoFile16: any;
  file17Name: string = '';
  archivoFile17: any;
  file18Name: string = '';
  archivoFile18: any;
  file19Name: string = '';
  archivoFile19: any;
  file20Name: string = '';
  archivoFile20: any;




  constructor(private dialogService: DialogService) { 

  }

  ngOnInit(): void {
  }

  get archivosCargados(): boolean{
    return this.archivoFile1 || this.archivoFile2 || this.archivoFile3 || this.archivoFile4 ||
    this.archivoFile5 || this.archivoFile6 || this.archivoFile7 || this.archivoFile8 || this.archivoFile9
    || this.archivoFile10 || this.archivoFile11 || this.archivoFile12 || this.archivoFile13 || this.archivoFile14
    || this.archivoFile15 || this.archivoFile16 || this.archivoFile17 || this.archivoFile18 || this.archivoFile19
    || this.archivoFile20;
  }
  removeFile1(){
    this.file1Placeholder.nativeElement.value = '';
    this.file1.nativeElement.value = '';
    this.file1Name = '';
    this.archivoFile1 = null;
  }

  removeFile2(){
    this.file2Placeholder.nativeElement.value = '';
    this.file2.nativeElement.value = '';
    this.file2Name = '';
    this.archivoFile2 = null;
  }

  removeFile3(){
    this.file3Placeholder.nativeElement.value = '';
    this.file3.nativeElement.value = '';
    this.file3Name = '';
    this.archivoFile3 = null;
  }

  removeFile4(){
    this.file4Placeholder.nativeElement.value = '';
    this.file4.nativeElement.value = '';
    this.file4Name = '';
    this.archivoFile4 = null;
  }

  removeFile5(){
    this.file5Placeholder.nativeElement.value = '';
    this.file5.nativeElement.value = '';
    this.file5Name = '';
    this.archivoFile5 = null;
  }

  removeFile6(){
    this.file6Placeholder.nativeElement.value = '';
    this.file6.nativeElement.value = '';
    this.file6Name = '';
    this.archivoFile6 = null;
  }

  removeFile7(){
    this.file7Placeholder.nativeElement.value = '';
    this.file7.nativeElement.value = '';
    this.file7Name = '';
    this.archivoFile7 = null;
  }

  removeFile8(){
    this.file8Placeholder.nativeElement.value = '';
    this.file8.nativeElement.value = '';
    this.file8Name = '';
    this.archivoFile8 = null;
  }

  removeFile9(){
    this.file9Placeholder.nativeElement.value = '';
    this.file9.nativeElement.value = '';
    this.file9Name = '';
    this.archivoFile9 = null;
  }

  removeFile10(){
    this.file10Placeholder.nativeElement.value = '';
    this.file10.nativeElement.value = '';
    this.file10Name = '';
    this.archivoFile10 = null;
  }

  removeFile11(){
    this.file11Placeholder.nativeElement.value = '';
    this.file11.nativeElement.value = '';
    this.file11Name = '';
    this.archivoFile11 = null;
  }

  removeFile12(){
    this.file12Placeholder.nativeElement.value = '';
    this.file12.nativeElement.value = '';
    this.file12Name = '';
    this.archivoFile12 = null;
  }

  removeFile13(){
    this.file13Placeholder.nativeElement.value = '';
    this.file13.nativeElement.value = '';
    this.file13Name = '';
    this.archivoFile13 = null;
  }

  removeFile14(){
    this.file14Placeholder.nativeElement.value = '';
    this.file14.nativeElement.value = '';
    this.file14Name = '';
    this.archivoFile14 = null;
  }

  removeFile15(){
    this.file15Placeholder.nativeElement.value = '';
    this.file15.nativeElement.value = '';
    this.file15Name = '';
    this.archivoFile15 = null;
  }

  removeFile16(){
    this.file16Placeholder.nativeElement.value = '';
    this.file16.nativeElement.value = '';
    this.file16Name = '';
    this.archivoFile16 = null;
  }

  removeFile17(){
    this.file17Placeholder.nativeElement.value = '';
    this.file17.nativeElement.value = '';
    this.file17Name = '';
    this.archivoFile17 = null;
  }

  removeFile18(){
    this.file18Placeholder.nativeElement.value = '';
    this.file18.nativeElement.value = '';
    this.file18Name = '';
    this.archivoFile18 = null;
  }

  removeFile19(){
    this.file19Placeholder.nativeElement.value = '';
    this.file19.nativeElement.value = '';
    this.file19Name = '';
    this.archivoFile19 = null;
  }

  removeFile20(){
    this.file20Placeholder.nativeElement.value = '';
    this.file20.nativeElement.value = '';
    this.file20Name = '';
    this.archivoFile20 = null;
  }

  changeFile1(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile1();
      return;
    }
     
    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile1();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file1Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile1 = reader.result || null;
    };
  }

  changeFile2(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile2();
      return;
    }
     
     

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile2();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file2Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile2 = reader.result || null;
    };
  }

  changeFile3(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile3();
      return;
    }
     
     

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile3();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file3Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile3 = reader.result || null;
    };
  }

  changeFile4(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile4();
      return;
    }
     
  

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile4();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file4Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile4 = reader.result || null;
    };
  }

  changeFile5(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile5();
      return;
    }
     
     

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile5();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file5Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile5 = reader.result || null;
    };
  }

  changeFile6(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile6();
      return;
    }
     
    
    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile6();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file6Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile6 = reader.result || null;
    };
  }

  changeFile7(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile7();
      return;
    }
     
     

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile7();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file7Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile7 = reader.result || null;
    };
  }

  changeFile8(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile8();
      return;
    }
     
    

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile8();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file8Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile8 = reader.result || null;
    };
  }

  changeFile9(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile9();
      return;
    }
     
    

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile9();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file9Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile9 = reader.result || null;
    };
  }

  changeFile10(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile10();
      return;
    }
     
    

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile10();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file10Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile10 = reader.result || null;
    };
  }

  changeFile11(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile11();
      return;
    }
     
    

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile11();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file11Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile11 = reader.result || null;
    };
  }

  changeFile12(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile12();
      return;
    }
     
    

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile12();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file12Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile12 = reader.result || null;
    };
  }

  changeFile13(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile13();
      return;
    }
     
    

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile13();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file13Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile13 = reader.result || null;
    };
  }

  changeFile14(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile14();
      return;
    }
     
    

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile14();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file14Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile14 = reader.result || null;
    };
  }

  changeFile15(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile15();
      return;
    }
     
    

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile15();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file15Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile15 = reader.result || null;
    };
  }

  changeFile16(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile16();
      return;
    }
     
    

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile16();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file16Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile16 = reader.result || null;
    };
  }

  changeFile17(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile17();
      return;
    }
     
    

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile17();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file17Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile17 = reader.result || null;
    };
  }

  changeFile18(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile18();
      return;
    }
     
    

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile18();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file18Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile18 = reader.result || null;
    };
  }

  changeFile19(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile19();
      return;
    }
     
    

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile19();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file19Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile19 = reader.result || null;
    };
  }

  changeFile20(event: any): void{
    const file = event.target.files[0];

    if(!file || file.length <= 0){
      this.removeFile20();
      return;
    }
     
    

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / (1024*1024);
      this.removeFile20();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
      return;
    }

    this.file20Name = file.name;

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.archivoFile20 = reader.result || null;
    };
  }



}