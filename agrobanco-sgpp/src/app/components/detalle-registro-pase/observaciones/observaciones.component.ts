import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Estados, Perfiles } from 'src/app/helpers/constantes';
import { RegistroPaseService } from 'src/app/services/registro-pase.service';
import { Pase } from '../../models/pase.interface';

@Component({
    selector: 'app-observaciones',
    templateUrl: './observaciones.component.html',
    styleUrls: ['./observaciones.component.css']
})
export class ObservacionesComponent implements OnInit, OnChanges {
  observaciones! : FormGroup;
  CondicionesEspeciales:string = '';
  Compilacion!: number;
  isRegistro: boolean= true;

  @Input() pase! : Pase

  eANA: Perfiles = Perfiles.ANA;
  cPerfil!: string;

  constructor(private registroPaseServices : RegistroPaseService, private formBuilder: FormBuilder) { 
    
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.pase){
      this.ObtenerObservaciones(this.pase.codPaseProduccion);
      this.isRegistro=true;
    }else{
      this.isRegistro=false;
    }
  }

  ngOnInit(): void {
    this.cPerfil = localStorage.getItem('perfil')!;
  
    this.observaciones = this.formBuilder.group({
      compilacion:['1'],  
      condicionesEspeciales: [{ value: '', disabled: true }]
    });

    this.f.compilacion.valueChanges.subscribe(
      val => {        
        if(val == 1){
          this.f.condicionesEspeciales.setValue('');
          this.f.condicionesEspeciales.clearValidators();
          this.f.condicionesEspeciales.setValidators([]);
          this.f.condicionesEspeciales.disable();
        }else{
          this.f.condicionesEspeciales.enable();
          this.f.condicionesEspeciales.setValidators([Validators.required]);
        }
        this.f.condicionesEspeciales.updateValueAndValidity();
      }
    )
  }

  ObtenerObservaciones(codPase : number): void{
    this.registroPaseServices.ObtenerObservaciones(codPase).subscribe((result:any)=>{    
      this.Compilacion = result.response.compilacion;
      this.CondicionesEspeciales = result.response.condicionesEspeciales;

      this.f.compilacion.setValue(String(this.Compilacion));
      this.f.condicionesEspeciales.setValue(this.CondicionesEspeciales);

      if(this.cPerfil !== this.eANA || this.pase.estado !== Estados.DevAnalista){
        this.f.compilacion.disable();
        this.f.condicionesEspeciales.disable();
      }

    });
  }

  get f(){
    return this.observaciones?.controls;
  }


}