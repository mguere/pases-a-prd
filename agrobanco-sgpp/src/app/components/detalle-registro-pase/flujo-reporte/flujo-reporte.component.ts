import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { RegistroPaseService } from 'src/app/services/registro-pase.service';
import { Pase } from '../../models/pase.interface';

@Component({
    selector: 'app-flujo-reporte',
    templateUrl: './flujo-reporte.component.html',
    styleUrls: ['./flujo-reporte.component.css']
})
export class FlujoReporteComponent  implements OnInit, OnChanges{
  formBuilder: FormBuilder = new FormBuilder();
  flujo! : FormGroup;
  CondicionesEspeciales:any = '';
  datosFlujo!: string;
  ldatosFlujo: any [] = [];
  
  @Input() pase!: Pase;

  constructor(private registroService : RegistroPaseService) { }
  ngOnChanges(changes: SimpleChanges): void {
    if(this.pase){
      this.obtenerDatosFlujo(this.pase.codPaseProduccion);

    }
  }

  ngOnInit(): void {
    
    this.flujo = this.formBuilder.group({
      descripcionFlujo: [''],
    });
  }

  obtenerDatosFlujo(codPase: number):void{

    this.registroService.ObtenerDatosFlujo(codPase).subscribe((result : any)=>{
      this.ldatosFlujo = result.response as [];

      this.datosFlujo="";
      
      for(let i=0; i<this.ldatosFlujo.length; i++){

        this.datosFlujo += this.ldatosFlujo[i]+"\n \n";
      }

      this.flujo.controls.descripcionFlujo.setValue(this.datosFlujo);

    });

  }

}