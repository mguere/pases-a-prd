import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Estados, Perfiles, TipoDerivacion, TipoObjeto, TipoPlataforma } from 'src/app/helpers/constantes';
import { DialogService } from 'src/app/services/comun/dialog.service';
import { RegistroPaseService } from 'src/app/services/registro-pase.service';
import { PaseWorkflow } from '../models/pase-workflow.interface';
import { Pase } from '../models/pase.interface';
import { Archivo, Objeto, RegPaseRequest, Sustento } from '../models/reg-pase-request.interface';
import { ArchivoComponent } from './archivo/archivo.component';
import { ComentarioDialogComponent } from './comentario-dialog/comentario-dialog.component';
import { ComentarioComponent } from './comentario/comentario.component';
import { DatosGeneralesComponent } from './datos-generales/datos-generales.component';
import { ObjetosComponent } from './objetos/objetos.component';
import { ObservacionesComponent } from './observaciones/observaciones.component';
import { SustentoComponent } from './sustento/sustento.component';

@Component({
    selector: 'app-detalle-registro-pase',
    templateUrl: './detalle-registro-pase.component.html',
    styleUrls: ['./detalle-registro-pase.component.css']
})
export class DetalleRegistroPase implements OnInit {

  loading: boolean = false;
  title: string = '';
  tipoPase!: number;
  idPase: number = 0;
  pase!: Pase;
  estado: number = -1;

  @ViewChild('datosGeneralesComp') datosGeneralesComp!: DatosGeneralesComponent;
  @ViewChild('archivoComp') archivoComp!: ArchivoComponent;
  @ViewChild('objetosComp') objetosComp!: ObjetosComponent;
  @ViewChild('sustentoComp') sustentoComp!: SustentoComponent;
  @ViewChild('comentarioComp') comentarioComp!: ComentarioComponent;
  @ViewChild('observacionesComp') observacionesComp!: ObservacionesComponent

  cPerfil: any = '';
  eANA: Perfiles = Perfiles.ANA;
  eEJS: Perfiles = Perfiles.EJS;
  eCAL: Perfiles = Perfiles.CAL;
  eSoporte: Perfiles = Perfiles.Soporte;

  eAplicacionWeb: TipoPlataforma = TipoPlataforma.AplicacionWeb;
  eAS400_IBS: TipoPlataforma = TipoPlataforma.AS400_IBS;
  PaginasWeb: TipoPlataforma = TipoPlataforma.PaginasWeb;
  Spring: TipoPlataforma = TipoPlataforma.Spring;

  constructor(
    private activatedRoute: ActivatedRoute,
    private registroPaseService: RegistroPaseService,
    private router: Router,
    private dialogService: DialogService,
    private matDialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.cPerfil = localStorage.getItem('perfil');

    this.activatedRoute.params.subscribe((params: Params) => {
      if(params.idTipoPlataforma){ // new form
        const tipoPase = +params.idTipoPlataforma;
        this.setTitle(tipoPase);
        this.tipoPase = tipoPase;
      }else if(params.idPase){ // view details and edition
        this.idPase = +params.idPase;
        this.obtenerDatosGenerales();        
      }
    })
  }

  get permisosBotones(){
    if(!this.pase) return false;

    return (this.cPerfil === this.eEJS && this.pase.estado === Estados.DerEjecutivo)
        || (this.cPerfil === this.eCAL && this.pase.estado === Estados.DerCalidad)
        || (this.cPerfil === this.eSoporte && this.pase.estado === Estados.DerSoporte);
  }

  private obtenerDatosGenerales(){
    this.loading = true;
        this.registroPaseService.ObtenerPasePorCodigo(this.idPase).subscribe(
          data => {
            this.loading = false;
            const pase = data.response as Pase;
            this.setTitle(pase.tipoPase);
            this.pase = pase;
            this.tipoPase = pase.tipoPase;
            this.estado = pase.estado;
          },
          err => {
            this.loading = false;
            console.log(err);
          }
        )
  }
    
  private setTitle(idTipoPlataforma: number){
    switch(idTipoPlataforma){
      case TipoPlataforma.AplicacionWeb:
        this.title = 'APLICACIONES WEB';
        break;
      case TipoPlataforma.AS400_IBS:
        this.title = 'SISTEMA IBS';
        break;
      case TipoPlataforma.PaginasWeb:
        this.title = 'PÁGINA WEB';
        break;
      case TipoPlataforma.Spring:
        this.title = 'SPRING';
        break;
    }
  }

  private get listSustentosRequest(){
    const lstSustentos: Sustento[] = [];

    if(!this.sustentoComp) return lstSustentos;

    if(this.sustentoComp.file1Name && this.sustentoComp.archivoFile1){
      const sustento: Sustento = { Nombre: this.sustentoComp.file1Name, Archivo: this.sustentoComp.archivoFile1, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    if(this.sustentoComp.file2Name && this.sustentoComp.archivoFile2){
      const sustento: Sustento = { Nombre: this.sustentoComp.file2Name, Archivo: this.sustentoComp.archivoFile2, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    if(this.sustentoComp.file3Name && this.sustentoComp.archivoFile3){
      const sustento: Sustento = { Nombre: this.sustentoComp.file3Name, Archivo: this.sustentoComp.archivoFile3, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    if(this.sustentoComp.file4Name && this.sustentoComp.archivoFile4){
      const sustento: Sustento = { Nombre: this.sustentoComp.file4Name, Archivo: this.sustentoComp.archivoFile4, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    if(this.sustentoComp.file5Name && this.sustentoComp.archivoFile5){
      const sustento: Sustento = { Nombre: this.sustentoComp.file5Name, Archivo: this.sustentoComp.archivoFile5, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    if(this.sustentoComp.file6Name && this.sustentoComp.archivoFile6){
      const sustento: Sustento = { Nombre: this.sustentoComp.file6Name, Archivo: this.sustentoComp.archivoFile6, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    if(this.sustentoComp.file7Name && this.sustentoComp.archivoFile7){
      const sustento: Sustento = { Nombre: this.sustentoComp.file7Name, Archivo: this.sustentoComp.archivoFile7, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    if(this.sustentoComp.file8Name && this.sustentoComp.archivoFile8){
      const sustento: Sustento = { Nombre: this.sustentoComp.file8Name, Archivo: this.sustentoComp.archivoFile8, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    if(this.sustentoComp.file9Name && this.sustentoComp.archivoFile9){
      const sustento: Sustento = { Nombre: this.sustentoComp.file9Name, Archivo: this.sustentoComp.archivoFile9, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    if(this.sustentoComp.file10Name && this.sustentoComp.archivoFile10){
      const sustento: Sustento = { Nombre: this.sustentoComp.file10Name, Archivo: this.sustentoComp.archivoFile10, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    if(this.sustentoComp.file11Name && this.sustentoComp.archivoFile11){
      const sustento: Sustento = { Nombre: this.sustentoComp.file11Name, Archivo: this.sustentoComp.archivoFile11, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    if(this.sustentoComp.file12Name && this.sustentoComp.archivoFile12){
      const sustento: Sustento = { Nombre: this.sustentoComp.file12Name, Archivo: this.sustentoComp.archivoFile12, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    if(this.sustentoComp.file13Name && this.sustentoComp.archivoFile13){
      const sustento: Sustento = { Nombre: this.sustentoComp.file13Name, Archivo: this.sustentoComp.archivoFile13, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    if(this.sustentoComp.file14Name && this.sustentoComp.archivoFile14){
      const sustento: Sustento = { Nombre: this.sustentoComp.file14Name, Archivo: this.sustentoComp.archivoFile14, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    if(this.sustentoComp.file15Name && this.sustentoComp.archivoFile15){
      const sustento: Sustento = { Nombre: this.sustentoComp.file15Name, Archivo: this.sustentoComp.archivoFile15, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    if(this.sustentoComp.file16Name && this.sustentoComp.archivoFile16){
      const sustento: Sustento = { Nombre: this.sustentoComp.file16Name, Archivo: this.sustentoComp.archivoFile16, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    if(this.sustentoComp.file17Name && this.sustentoComp.archivoFile17){
      const sustento: Sustento = { Nombre: this.sustentoComp.file17Name, Archivo: this.sustentoComp.archivoFile17, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    if(this.sustentoComp.file18Name && this.sustentoComp.archivoFile18){
      const sustento: Sustento = { Nombre: this.sustentoComp.file18Name, Archivo: this.sustentoComp.archivoFile18, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    if(this.sustentoComp.file19Name && this.sustentoComp.archivoFile19){
      const sustento: Sustento = { Nombre: this.sustentoComp.file19Name, Archivo: this.sustentoComp.archivoFile19, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    if(this.sustentoComp.file20Name && this.sustentoComp.archivoFile20){
      const sustento: Sustento = { Nombre: this.sustentoComp.file20Name, Archivo: this.sustentoComp.archivoFile20, CodLaserFiche: 0 };
      lstSustentos.push(sustento);
    }

    return lstSustentos;
  }

  private get listArchivosRequest(){
    const lstArchivos: Archivo[] = [];

    if(!this.archivoComp) return lstArchivos;

    const tipoPase = this.datosGeneralesComp.tipoPase;
    const archivos = this.archivoComp.archivos.value;

    if(tipoPase === TipoPlataforma.AplicacionWeb){

      if(archivos.Archivo1){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo1, Origen: '', Destino: '', Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo2){
        const oArchivo2: Archivo = { Nombre: archivos.Archivo2, Origen: '', Destino: '', Modulo: 0 };
        lstArchivos.push(oArchivo2);
      }

      if(archivos.Archivo3){
        const oArchivo3: Archivo = { Nombre: archivos.Archivo3, Origen: '', Destino: '', Modulo: 0 };
        lstArchivos.push(oArchivo3);
      }

      if(archivos.Archivo4){
        const oArchivo4: Archivo = { Nombre: archivos.Archivo4, Origen: '', Destino: '', Modulo: 0 };
        lstArchivos.push(oArchivo4);
      }

      if(archivos.Archivo5){
        const oArchivo5: Archivo = { Nombre: archivos.Archivo5, Origen: '', Destino: '', Modulo: 0 };
        lstArchivos.push(oArchivo5);
      }

      if(archivos.Archivo6){
        const oArchivo6: Archivo = { Nombre: archivos.Archivo6, Origen: '', Destino: '', Modulo: 0 };
        lstArchivos.push(oArchivo6);
      }

    }

    if(tipoPase === TipoPlataforma.PaginasWeb){

      if(archivos.Archivo1_PaginaWeb || archivos.Destino1_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo1_PaginaWeb, Origen: '', Destino: archivos.Destino1_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo2_PaginaWeb || archivos.Destino2_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo2_PaginaWeb, Origen: '', Destino: archivos.Destino2_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo3_PaginaWeb || archivos.Destino3_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo3_PaginaWeb, Origen: '', Destino: archivos.Destino3_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo4_PaginaWeb || archivos.Destino4_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo4_PaginaWeb, Origen: '', Destino: archivos.Destino4_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo5_PaginaWeb || archivos.Destino5_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo5_PaginaWeb, Origen: '', Destino: archivos.Destino5_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo6_PaginaWeb || archivos.Destino6_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo6_PaginaWeb, Origen: '', Destino: archivos.Destino6_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo7_PaginaWeb || archivos.Destino7_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo7_PaginaWeb, Origen: '', Destino: archivos.Destino7_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo8_PaginaWeb || archivos.Destino8_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo8_PaginaWeb, Origen: '', Destino: archivos.Destino8_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo9_PaginaWeb || archivos.Destino9_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo9_PaginaWeb, Origen: '', Destino: archivos.Destino9_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo10_PaginaWeb || archivos.Destino10_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo10_PaginaWeb, Origen: '', Destino: archivos.Destino10_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo11_PaginaWeb || archivos.Destino11_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo11_PaginaWeb, Origen: '', Destino: archivos.Destino11_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo12_PaginaWeb || archivos.Destino12_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo12_PaginaWeb, Origen: '', Destino: archivos.Destino12_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo13_PaginaWeb || archivos.Destino13_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo13_PaginaWeb, Origen: '', Destino: archivos.Destino13_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo14_PaginaWeb || archivos.Destino14_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo14_PaginaWeb, Origen: '', Destino: archivos.Destino14_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo15_PaginaWeb || archivos.Destino15_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo15_PaginaWeb, Origen: '', Destino: archivos.Destino15_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo16_PaginaWeb || archivos.Destino16_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo16_PaginaWeb, Origen: '', Destino: archivos.Destino16_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo17_PaginaWeb || archivos.Destino17_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo17_PaginaWeb, Origen: '', Destino: archivos.Destino17_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo18_PaginaWeb || archivos.Destino18_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo18_PaginaWeb, Origen: '', Destino: archivos.Destino18_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo19_PaginaWeb || archivos.Destino19_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo19_PaginaWeb, Origen: '', Destino: archivos.Destino19_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo20_PaginaWeb || archivos.Destino20_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo20_PaginaWeb, Origen: '', Destino: archivos.Destino20_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo21_PaginaWeb || archivos.Destino21_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo21_PaginaWeb, Origen: '', Destino: archivos.Destino21_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo22_PaginaWeb || archivos.Destino22_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo22_PaginaWeb, Origen: '', Destino: archivos.Destino22_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo23_PaginaWeb || archivos.Destino23_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo23_PaginaWeb, Origen: '', Destino: archivos.Destino23_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo24_PaginaWeb || archivos.Destino24_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo24_PaginaWeb, Origen: '', Destino: archivos.Destino24_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo25_PaginaWeb || archivos.Destino25_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo25_PaginaWeb, Origen: '', Destino: archivos.Destino25_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo26_PaginaWeb || archivos.Destino26_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo26_PaginaWeb, Origen: '', Destino: archivos.Destino26_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo27_PaginaWeb || archivos.Destino27_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo27_PaginaWeb, Origen: '', Destino: archivos.Destino27_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo28_PaginaWeb || archivos.Destino28_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo28_PaginaWeb, Origen: '', Destino: archivos.Destino28_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo29_PaginaWeb || archivos.Destino29_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo29_PaginaWeb, Origen: '', Destino: archivos.Destino29_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Archivo30_PaginaWeb || archivos.Destino30_PaginaWeb){
        const oArchivo1: Archivo = { Nombre: archivos.Archivo30_PaginaWeb, Origen: '', Destino: archivos.Destino30_PaginaWeb, Modulo: 0 };
        lstArchivos.push(oArchivo1);
      }

    }

    if(tipoPase === TipoPlataforma.Spring){

      if(archivos.Modulo1 || archivos.Fuente1_Spring || archivos.Destino1_Spring){
        const oArchivo1: Archivo = { Nombre: '', Origen: archivos.Fuente1_Spring, Destino: archivos.Destino1_Spring, Modulo: +archivos.Modulo1 };
        lstArchivos.push(oArchivo1);
      }

      if(archivos.Modulo2 || archivos.Fuente2_Spring || archivos.Destino2_Spring){
        const oArchivo2: Archivo = { Nombre: '', Origen: archivos.Fuente2_Spring, Destino: archivos.Destino2_Spring, Modulo: +archivos.Modulo2 };
        lstArchivos.push(oArchivo2);
      }

      if(archivos.Modulo3 || archivos.Fuente3_Spring || archivos.Destino3_Spring){
        const oArchivo3: Archivo = { Nombre: '', Origen: archivos.Fuente3_Spring, Destino: archivos.Destino3_Spring, Modulo: +archivos.Modulo3 };
        lstArchivos.push(oArchivo3);
      }

      if(archivos.Modulo1 || archivos.Fuente4_Spring || archivos.Destino4_Spring){
        const oArchivo4: Archivo = { Nombre: '', Origen: archivos.Fuente4_Spring, Destino: archivos.Destino4_Spring, Modulo: +archivos.Modulo4 };
        lstArchivos.push(oArchivo4);
      }

    }

    return lstArchivos;
  }

  private get listObjetosRequest(){
    const lstObjetos: Objeto[] = [];

    if(!this.objetosComp) return lstObjetos;

    const objetos = this.objetosComp.objetos.value;

    if(objetos.objeto1_nombre || objetos.objeto1_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QDDSSRC, Nombre: objetos.objeto1_nombre, Descripcion: objetos.objeto1_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto2_nombre || objetos.objeto2_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QCLSRC, Nombre: objetos.objeto2_nombre, Descripcion: objetos.objeto2_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto3_nombre || objetos.objeto3_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QRPGSRC, Nombre: objetos.objeto3_nombre, Descripcion: objetos.objeto3_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto4_nombre || objetos.objeto4_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QDDSSRC, Nombre: objetos.objeto4_nombre, Descripcion: objetos.objeto4_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto5_nombre || objetos.objeto5_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QCLSRC, Nombre: objetos.objeto5_nombre, Descripcion: objetos.objeto5_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto6_nombre || objetos.objeto6_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QRPGSRC, Nombre: objetos.objeto6_nombre, Descripcion: objetos.objeto6_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto7_nombre || objetos.objeto7_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QDDSSRC, Nombre: objetos.objeto7_nombre, Descripcion: objetos.objeto7_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto8_nombre || objetos.objeto8_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QCLSRC, Nombre: objetos.objeto8_nombre, Descripcion: objetos.objeto8_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto9_nombre || objetos.objeto9_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QRPGSRC, Nombre: objetos.objeto9_nombre, Descripcion: objetos.objeto9_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto10_nombre || objetos.objeto10_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QDDSSRC, Nombre: objetos.objeto10_nombre, Descripcion: objetos.objeto10_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto11_nombre || objetos.objeto11_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QCLSRC, Nombre: objetos.objeto11_nombre, Descripcion: objetos.objeto11_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto12_nombre || objetos.objeto12_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QRPGSRC, Nombre: objetos.objeto12_nombre, Descripcion: objetos.objeto12_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto13_nombre || objetos.objeto13_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QDDSSRC, Nombre: objetos.objeto13_nombre, Descripcion: objetos.objeto13_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto14_nombre || objetos.objeto14_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QCLSRC, Nombre: objetos.objeto14_nombre, Descripcion: objetos.objeto14_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto15_nombre || objetos.objeto15_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QRPGSRC, Nombre: objetos.objeto15_nombre, Descripcion: objetos.objeto15_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto16_nombre || objetos.objeto16_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QDDSSRC, Nombre: objetos.objeto16_nombre, Descripcion: objetos.objeto16_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto17_nombre || objetos.objeto17_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QCLSRC, Nombre: objetos.objeto17_nombre, Descripcion: objetos.objeto17_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto18_nombre || objetos.objeto18_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QRPGSRC, Nombre: objetos.objeto18_nombre, Descripcion: objetos.objeto18_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto19_nombre || objetos.objeto19_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QDDSSRC, Nombre: objetos.objeto19_nombre, Descripcion: objetos.objeto19_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto20_nombre || objetos.objeto20_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QCLSRC, Nombre: objetos.objeto20_nombre, Descripcion: objetos.objeto20_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto21_nombre || objetos.objeto21_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QRPGSRC, Nombre: objetos.objeto21_nombre, Descripcion: objetos.objeto21_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto22_nombre || objetos.objeto22_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QDDSSRC, Nombre: objetos.objeto22_nombre, Descripcion: objetos.objeto22_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto23_nombre || objetos.objeto23_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QCLSRC, Nombre: objetos.objeto23_nombre, Descripcion: objetos.objeto23_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto24_nombre || objetos.objeto24_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QRPGSRC, Nombre: objetos.objeto24_nombre, Descripcion: objetos.objeto24_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto25_nombre || objetos.objeto25_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QDDSSRC, Nombre: objetos.objeto25_nombre, Descripcion: objetos.objeto25_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto26_nombre || objetos.objeto26_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QCLSRC, Nombre: objetos.objeto26_nombre, Descripcion: objetos.objeto26_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto27_nombre || objetos.objeto27_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QRPGSRC, Nombre: objetos.objeto27_nombre, Descripcion: objetos.objeto27_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto28_nombre || objetos.objeto28_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QDDSSRC, Nombre: objetos.objeto28_nombre, Descripcion: objetos.objeto28_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto29_nombre || objetos.objeto29_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QCLSRC, Nombre: objetos.objeto29_nombre, Descripcion: objetos.objeto29_descripcion };
      lstObjetos.push(item);
    }

    if(objetos.objeto30_nombre || objetos.objeto30_descripcion){
      const item: Objeto = { Tipo: TipoObjeto.QRPGSRC, Nombre: objetos.objeto30_nombre, Descripcion: objetos.objeto30_descripcion };
      lstObjetos.push(item);
    }

    return lstObjetos;
  }

  grabarEnviarEjecutivo(){

    if(this.datosGeneralesComp.datosGenerales.invalid){
      this.datosGeneralesComp.datosGenerales.markAllAsTouched();
      return;
    }

    if(this.observacionesComp.observaciones.invalid){
      this.observacionesComp.observaciones.markAllAsTouched();
      return;
    }

    if(!this.pase && !this.sustentoComp.archivosCargados){
      this.dialogService.openMsgErrorDialog('Debe cargar al menos un documento adjunto.');
      return;
    }

    if(this.comentarioComp.comentarios.invalid){
      this.comentarioComp.comentarios.markAllAsTouched();
      return;
    }

    const tipoPase = this.datosGeneralesComp.tipoPase;
    const datosGenerales = this.datosGeneralesComp.datosGenerales.value;
    const usuarioWeb = localStorage.getItem('UsuarioWeb')!;

    let motivoPase = Number(datosGenerales.motivoPase);
    let libreriaFuente = +datosGenerales.libreriaFuente;
    let libreriaObjeto = +datosGenerales.libreriaObjeto;
    let libreriaObjetoPFLF = +datosGenerales.libreriaObjetoPFLF;

    if(tipoPase !== TipoPlataforma.AS400_IBS){
      motivoPase = 0;
      libreriaFuente = 0;
      libreriaObjeto = 0;
      libreriaObjetoPFLF = 0;
    }

    if(tipoPase == TipoPlataforma.AS400_IBS){
      datosGenerales.paseInterno = -1;
    }

    const regPaseRequest: RegPaseRequest = {
      CodPaseProduccion: this.idPase,
      TipoPase: tipoPase,
      Ticket: datosGenerales.ticket,
      Descripcion: datosGenerales.descripcion,
      UbicacionFuente: datosGenerales.fuente,
      PaseInterno: +datosGenerales.paseInterno,
      MotivoPase: motivoPase,
      Compilacion: +this.observacionesComp.observaciones.value.compilacion, 
      CondicionesEspeciales: this.observacionesComp.observaciones.value.condicionesEspeciales, 
      LibreriaFuente: libreriaFuente,
      LibreriaObjetoOtros: libreriaObjeto,
      LibreriaObjetoPFyLF: libreriaObjetoPFLF,
      Analista: datosGenerales.analista,
      UsuarioRegistro: usuarioWeb,
      Comentario: this.comentarioComp.comentarios.value.comentario,
      Objetos: this.listObjetosRequest,
      Archivos: this.listArchivosRequest,
      Sustentos: this.listSustentosRequest
    }

    //console.log('request: ', regPaseRequest);
    if(this.cPerfil === this.eANA && this.pase && this.pase.estado === Estados.DevAnalista){
      const dialogRef = this.matDialog.open(ComentarioDialogComponent, {
        width: '500px',
        panelClass: 'confirm-dialog-container',
        disableClose: true
      });

      dialogRef.afterClosed().subscribe(
        comentario => {
          if(!comentario) return;

          regPaseRequest.Comentario = comentario;
          this.confirmInsertOrUpdatePase(regPaseRequest);
        }
      )
    }else{
      this.confirmInsertOrUpdatePase(regPaseRequest);
    }
  }

  private confirmInsertOrUpdatePase(regPaseRequest: RegPaseRequest){
    this.dialogService.openConfirmDialog('¿Está seguro de grabar y enviar a ejecutivo?')
      .afterClosed().subscribe(
        flag => {
          if(flag){

            if(regPaseRequest.CodPaseProduccion === 0){
              this.insertarPase(regPaseRequest);
            }else{
              this.actualizarPase(regPaseRequest);
            }

          }
        }
      )
  }

  private insertarPase(regPaseRequest: RegPaseRequest){
    this.loading = true;
    this.registroPaseService.InsertarPase(regPaseRequest)
                .subscribe(
                  (res: any) => {
                    const result = res.response;
                    this.loading = false;
                    if(result.resultado){
                      this.dialogService.openMsgSuccessDialogWithActionRedirect(result.mensaje,'/gestion-pase-produccion');
                    }else{
                      this.dialogService.openMsgErrorDialog(result.mensaje);
                    }
                  },
                  err => {
                    this.loading = false;
                    console.log(err)
                  }
                )
  }

  private actualizarPase(regPaseRequest: RegPaseRequest){
    this.loading = true;
    this.registroPaseService.ActualizarPase(regPaseRequest)
                .subscribe(
                  (res: any) => {
                    const result = res.response;
                    this.loading = false;
                    if(result.resultado){
                      this.dialogService.openMsgSuccessDialogWithActionRedirect(result.mensaje,'/gestion-pase-produccion');
                    }else{
                      this.dialogService.openMsgErrorDialog(result.mensaje);
                    }
                  },
                  err => {
                    this.loading = false;
                    console.log(err)
                  }
                )
  }

  salir(){
    this.router.navigate(['/gestion-pase-produccion'])
  }

  derivarPase(type: number){
    const dialogRef = this.matDialog.open(ComentarioDialogComponent, {
      width: '500px',
      panelClass: 'confirm-dialog-container',
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(
      comentario => {

        if(!comentario) return;

        let messageConfirm = '';
        if(type === TipoDerivacion.Aprobar){
          messageConfirm = '¿Está seguro de Aprobar?';
        }else if(type === TipoDerivacion.Rechazar){
          messageConfirm = '¿Está seguro de rechazar el pase a producción?';
        }else if(type === TipoDerivacion.Anular){
          messageConfirm = '¿Está seguro de Anular?';
        }

        this.dialogService.openConfirmDialog(messageConfirm)
            .afterClosed().subscribe(
              flag => {
                if(flag){

                  const req: PaseWorkflow = {
                    CodPase: this.pase.codPaseProduccion,
                    Perfil: localStorage.getItem('perfil')!,
                    Comentario: comentario,
                    UsuarioRegistro: localStorage.getItem('UsuarioWeb')!
                  };
                  
                  if(type === TipoDerivacion.Aprobar){
                    this.aprobar(req);
                  }else if(type === TipoDerivacion.Rechazar){
                    this.rechazar(req);
                  }else if(type === TipoDerivacion.Anular){
                    this.anular(req);
                  }
                  
                }
              }
            )
      }
    )
  }

  aprobar(req: PaseWorkflow){
    this.loading = true;
    this.registroPaseService.AprobarPase(req)
        .subscribe(
          (data: any) => {
            this.loading = false;
            const result = data.response;
            if(result.resultado){
              this.dialogService.openMsgSuccessDialogWithActionRedirect(result.mensaje, '/gestion-pase-produccion');
            }else{
              this.dialogService.openMsgErrorDialog(result.mensaje);
            } 
          },
          err => {
            this.loading = false;
            console.log(err);
          }
        )
  }

  rechazar(req: PaseWorkflow){
    this.loading = true;
    this.registroPaseService.RechazarPase(req)
        .subscribe(
          (data: any) => {
            this.loading = false;
            const result = data.response;
            if(result.resultado){
              this.dialogService.openMsgSuccessDialogWithActionRedirect(result.mensaje, '/gestion-pase-produccion');
            }else{
              this.dialogService.openMsgErrorDialog(result.mensaje);
            } 
          },
          err => {
            this.loading = false;
            console.log(err);
          }
        )
  }

  anular(req: PaseWorkflow){
    this.loading = true;
    this.registroPaseService.AnularPase(req)
        .subscribe(
          (data: any) => {
            this.loading = false;
            const result = data.response;
            if(result.resultado){
              this.dialogService.openMsgSuccessDialogWithActionRedirect(result.mensaje, '/gestion-pase-produccion');
            }else{
              this.dialogService.openMsgErrorDialog(result.mensaje);
            } 
          },
          err => {
            this.loading = false;
            console.log(err);
          }
        )
  }

}
