import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-comentario-dialog',
  templateUrl: './comentario-dialog.component.html',
  styleUrls: ['./comentario-dialog.component.css']
})
export class ComentarioDialogComponent implements OnInit {

  comentario = new FormControl('', [Validators.required, this.noWhitespaceValidator]);

  constructor(
    private dialogRef: MatDialogRef<ComentarioDialogComponent>
  ) { }

  ngOnInit(): void {
  }

  aceptar(){
    if(this.comentario.invalid){
      return;
    }

    this.dialogRef.close(this.comentario.value);
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

}
