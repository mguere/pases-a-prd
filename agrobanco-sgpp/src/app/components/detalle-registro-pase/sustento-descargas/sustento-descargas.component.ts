import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { RegistroPaseService } from 'src/app/services/registro-pase.service';
import { Pase } from '../../models/pase.interface';
import { SustentoDetalle } from '../../models/sustento-detalle-interface';
import * as FileSaver from "file-saver";

@Component({
  selector: 'app-sustento-descargas',
  templateUrl: './sustento-descargas.component.html',
  styleUrls: ['./sustento-descargas.component.css']
})
export class SustentoDescargasComponent implements OnInit, OnChanges, OnDestroy {

  loading: boolean = false;

  @Input() pase!: Pase;

  lstSustentos: SustentoDetalle[] = [];
  sustentosSubs!: Subscription;

  constructor(
    private regPaseService: RegistroPaseService
  ) { }
  
  ngOnChanges(changes: SimpleChanges): void {
    if(this.pase){
      this.cargarSustentos();
    }
  }

  ngOnInit(): void {
  }

  cargarSustentos(){
    this.loading = true;

    this.sustentosSubs = this.regPaseService.ObtenerListaSustentos(this.pase.codPaseProduccion)
      .subscribe(
        data => {
          this.loading = false;
          this.lstSustentos = data.response as SustentoDetalle[];
          this.lstSustentos = this.lstSustentos.map(row => {
            row.nombreAbreviatura = row.nombre;
            if(row.nombre.length > 60)
              row.nombreAbreviatura = row.nombre.substring(0,60) + '...';

            return row;
          })
        },
        err => {
          this.loading = false;
          console.log(err);
        }
      )
  }

  descargar(s: SustentoDetalle){
    this.loading = true;
    this.regPaseService.DescargarSustento(s.codLaserFiche).subscribe(blob => {
      this.loading = false;
      FileSaver.saveAs(blob, s.nombre);
    },
    err => {
      this.loading = false;
      console.log(err);
    })
  }

  ngOnDestroy(): void {
    this.sustentosSubs?.unsubscribe();
  }

}
