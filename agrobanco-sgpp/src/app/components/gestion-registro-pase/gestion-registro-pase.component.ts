import { Component, OnInit, ViewChild, EventEmitter, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder, ValidatorFn } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from "@angular/router";
import { ExcelJsService } from 'src/app/services/comun/excel-js.service';
import { MatSort } from '@angular/material/sort';
//import { ParametroService } from 'src/app/services/parametro.service';
import { SecurityService } from 'src/app/services/security.service';

import { environment } from 'src/environments/environment';
import { Observable, Subject, forkJoin} from 'rxjs';
import { RegistroPaseConsulta } from '../models/registropase.interface';
import { RegistroPaseService } from 'src/app/services/registro-pase.service';
import * as moment from 'moment';
import { MatDialog } from '@angular/material/dialog';
import { NuevoFormularioDialogComponent } from '../nuevo-formulario-dialog/nuevo-formulario-dialog.component';
import { DatePipe } from '@angular/common';
import { Perfiles } from 'src/app/helpers/constantes';

// import { DialogService } from 'src/app/services/comun/dialog.service';
// import { DomService } from 'src/app/services/dom.service';

@Component({
  selector: 'app-gestion-registro-pase',
  templateUrl: './gestion-registro-pase.component.html',
  styleUrls: ['./gestion-registro-pase.component.css']
})
export class GestionRegistroPaseComponent implements OnInit, AfterViewInit {

  TokenSGS = localStorage.getItem('TokenSGS');

  busquedaFiltro! : FormGroup;
  ELEMENT_DATA! :RegistroPaseConsulta[];
  displayedColumns: string[] = ["idPase", "nombre", "descripcion", "creadoPor", 
  "aprobadoPor", "fechaRegistro", "estado", "pase", "detalle"];

  listaRegistroPase= new MatTableDataSource<RegistroPaseConsulta>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
    
  tiposPlataforma:any[] = []; 
  estados:any[] = [];  
  fechaRegistroInicioValor! : Date;
  fechaRegistroFinValor! : Date;  
  peticionesTerminadas: boolean = true;

  cPerfil: any = '';
  eANA: Perfiles = Perfiles.ANA;
  eEJS: Perfiles = Perfiles.EJS;
  eCAL: Perfiles = Perfiles.CAL;
  eSoporte: Perfiles = Perfiles.Soporte;



  loading: boolean = false;

  constructor(
    private registroPaseService : RegistroPaseService,
    private securityService: SecurityService,
    private router: Router,
    private excelJsService:ExcelJsService,
    private dialog: MatDialog
  ) {}

  ngAfterViewInit(): void {
    this.listaRegistroPase.paginator = this.paginator;
    this.listaRegistroPase.sort = this.sort;
  }

  ngOnInit(): void {

    this.inicializarFormulario();       
    this.loadPage();   
    this.cPerfil = localStorage.getItem('perfil');
  
  }
  
  loadPage(): void {

    this.peticionesTerminadas = false;
    const params = this.getRequest;

    forkJoin([
      
      this.registroPaseService.ObtenerTipoPlataformaLista(),
      this.registroPaseService.ObtenerEstadoLista(),
      this.registroPaseService.ObtenerPaseLista(params)
      
    ]).subscribe( (data:any) => {
           
        this.tiposPlataforma = data[0].response;      
        this.estados = data[1].response;  
        this.listaRegistroPase.data = data[2].response as RegistroPaseConsulta[];

        this.peticionesTerminadas = true;
    }, (error) =>{
      console.log(error);
      this.peticionesTerminadas = true;
    });

  }

  private inicializarFormulario()
  {   
    let fechaHoy = new Date();
    let f = new Date();
    let fechaInicioMes = new Date(f.setDate(fechaHoy.getDate() - 1));
    this.fechaRegistroInicioValor = fechaInicioMes;
    this.fechaRegistroFinValor = fechaHoy;  

    this.busquedaFiltro = new FormGroup({
      CodEstado : new FormControl('-1'),      
      CodTipoPlataforma : new FormControl('-1'),
      FechaRegistroIni: new FormControl(fechaInicioMes, [Validators.max(this.fechaRegistroFinValor.getTime())]),      
      FechaRegistroFin: new FormControl(fechaHoy, [Validators.min(this.fechaRegistroInicioValor.getTime())]) 
    });    
  } 

  
  
  buscarListaPases():void
  {
    
    if(this.busquedaFiltro.invalid)
    {
      return;
    }
    this.loading = true;
    this.peticionesTerminadas = false;

      const params = this.getRequest;

      this.registroPaseService.ObtenerPaseLista(params).subscribe((result:any) =>{
      this.listaRegistroPase.data = result.response as RegistroPaseConsulta[];
      this.peticionesTerminadas = true;
      this.loading = false;
    }, (error)=> {
      console.log(error);
      this.peticionesTerminadas = true;
    });
  }

  private get getRequest(){

    let FechaRegistroIni = '';
    let FechaRegistroFin = '';
    let date = new DatePipe('en-US'); 
    
    if (this.busquedaFiltro.controls["FechaRegistroIni"].value){  
      FechaRegistroIni = date.transform(this.busquedaFiltro.value.FechaRegistroIni, 'yyyyMMdd')!;
    }

    if(this.busquedaFiltro.controls["FechaRegistroFin"].value){
      FechaRegistroFin =date.transform(this.busquedaFiltro.value.FechaRegistroFin, 'yyyyMMdd')!;
    }

    const requestDto = {
      CodTipoPlataforma : +this.busquedaFiltro.controls["CodTipoPlataforma"].value,
      CodEstado : +this.busquedaFiltro.controls["CodEstado"].value,    
      FechaRegistroIni,
      FechaRegistroFin  
    };

    return requestDto;
  }
  
  ExportarExcel():void {
    if (this.busquedaFiltro.invalid) {
      return;
    }
    
    const requestDto = this.getRequest;
    this.loading = true;

    this.registroPaseService.ObtenerPaseLista(requestDto).subscribe(result=>{ 

        this.listaRegistroPase.data = result.response as RegistroPaseConsulta[];      

        const FechaActual = new Date();
        const AnioActual = FechaActual.getFullYear();
        const MesActual = (FechaActual.getMonth()<=9? '0'+ (FechaActual.getMonth()+1): FechaActual.getMonth() + 1); //FechaActual.getMonth() + 1;
        const DiaActual = (FechaActual.getDate()<=9? '0'+FechaActual.getDate(): FechaActual.getDate());
        const HoraActual = (FechaActual.getHours()<=9? '0'+FechaActual.getHours(): FechaActual.getHours()); //FechaActual.getHours();
        const MinutoActual = (FechaActual.getMinutes()<=9? '0'+FechaActual.getMinutes(): FechaActual.getMinutes()); //FechaActual.getMinutes();
        const SegundosActual = (FechaActual.getSeconds()<=9? '0'+FechaActual.getSeconds(): FechaActual.getSeconds()); //FechaActual.getMinutes(); //FechaActual.getSeconds();

        const Archivo = "PASE_PRODUCCION_" + AnioActual + MesActual + DiaActual + "T" + HoraActual + MinutoActual + SegundosActual;
        const Sheet = "Hoja1";
        const header = ["ID", "Nombre", "Descripción", "Creador", "Aprobado por", "Fecha de creación", "Estado", "Pase interno"];
        const header_width = [20,20,20,20,20,20,25,25];
        let body = [];
        for (let index = 0; index < result.response.length; index++) {
          const datos = result.response[index];
          var idPase = (datos.idPase==null?'': datos.idPase); 
          var nombre = (datos.nombre==null?'': datos.nombre);
          var descripcion = (datos.descripcion==null?'': datos.descripcion);
          var creadoPor = (datos.creadoPor==null?'': datos.creadoPor);  
          var aprobadoPor = (datos.aprobadoPor==null?'': datos.aprobadoPor);    
          var fechaRegistro = (datos.fechaRegistro==null?'': moment(datos.fechaRegistro).format("DD/MM/YYYY"));                 
          var estado = (datos.estado==null?'': datos.estado);
          var pase = (datos.pase==null?'': datos.pase);

          var datoarray = [idPase,nombre,descripcion,creadoPor,aprobadoPor,fechaRegistro,estado,pase]
          body.push(datoarray)
        }

        this.excelJsService.DescargarExcelGenerico(Archivo,Sheet,header,body,header_width);
    this.loading = false;

    },(err)=>{
      console.log(err)
    });

  }

  nuevoFormulario(){
    const dialogRef = this.dialog.open(NuevoFormularioDialogComponent, {
      width: '400px',
      panelClass: 'confirm-dialog-container',
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(
      tipoPlataforma => {
        if(tipoPlataforma){
          this.router.navigate(['crear-registro-pase', tipoPlataforma])
        }
      }
    )
  }

  verDetalle(idPase: number){
    this.router.navigate(['detalle-registro-pase', idPase]);
  }

}
