import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { RegistroPaseService } from 'src/app/services/registro-pase.service';

@Component({
  selector: 'app-nuevo-formulario-dialog',
  templateUrl: './nuevo-formulario-dialog.component.html',
  styleUrls: ['./nuevo-formulario-dialog.component.css']
})
export class NuevoFormularioDialogComponent implements OnInit, OnDestroy {

  loading: boolean = false;
  listTipoPlataforma: any[] = [];
  tipoPlataformaSubs!: Subscription;
  CodTipoPlataforma: FormControl = new FormControl(null, Validators.required);

  constructor(
    private registroPaseService: RegistroPaseService,
    private dialogRef: MatDialogRef<NuevoFormularioDialogComponent>
  ) { }
  
  ngOnInit(): void {
    this.loading = true;
    this.tipoPlataformaSubs = this.registroPaseService.ObtenerTipoPlataformaLista().subscribe(
      data => {
        this.listTipoPlataforma = data.response;
      },
      err => {
        console.log(err);
      },
      () => {
        this.loading = false;
      }
    )
  }

  aceptar(){
    if(this.CodTipoPlataforma.valid){
      this.dialogRef.close(this.CodTipoPlataforma.value);
    }
  }

  ngOnDestroy(): void {
    this.tipoPlataformaSubs.unsubscribe();
  }

}
