import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { MatSelectModule } from '@angular/material/select';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { SidebarComponent } from './components/shared/sidebar/sidebar.component';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { HttpClientModule } from '@angular/common/http';

import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import {MatStepperModule} from '@angular/material/stepper';
//import { FormatoFechasPipe } from './pipes/formato-fechas.pipe';
import { DialogConfirmComponent } from './components/shared/dialog-confirm/dialog-confirm.component';
import { MsgSucessComponent } from './components/shared/msg-sucess/msg-sucess.component';

import {MatRadioModule} from '@angular/material/radio';
import {MatTooltipModule} from '@angular/material/tooltip';
import { MatSortModule } from '@angular/material/sort';
import { MatButtonModule } from '@angular/material/button';

import { AuthGuard } from './guards/auth.guard';
import { HomeComponent } from './components/home/home.component';
import { IndexComponent } from './components/redirect/index/index.component';
import { ErrorComponent } from './components/error/error.component';
import { LoadingComponent } from './components/shared/loading/loading.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { MsgErrorComponent } from './components/shared/msg-error/msg-error.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { GestionRegistroPaseComponent } from './components/gestion-registro-pase/gestion-registro-pase.component';
import { DetalleRegistroPase } from './components/detalle-registro-pase/detalle-registro-pase.component';
import { DatosGeneralesComponent } from './components/detalle-registro-pase/datos-generales/datos-generales.component';
import { ArchivoComponent } from './components/detalle-registro-pase/archivo/archivo.component';
import { ObservacionesComponent } from './components/detalle-registro-pase/observaciones/observaciones.component';
import { ObjetosComponent } from './components/detalle-registro-pase/objetos/objetos.component';
import { InformacionSoporteComponent } from './components/detalle-registro-pase/Informacion-soporte/informacion-soporte.component';
import { SustentoComponent } from './components/detalle-registro-pase/sustento/sustento.component';
import { ComentarioComponent } from './components/detalle-registro-pase/comentario/comentario.component';
import { FlujoReporteComponent } from './components/detalle-registro-pase/flujo-reporte/flujo-reporte.component';
import { NuevoFormularioDialogComponent } from './components/nuevo-formulario-dialog/nuevo-formulario-dialog.component';
import { SimpleSpacesDirective } from './directives/simple-spaces.directive';
import { SustentoDescargasComponent } from './components/detalle-registro-pase/sustento-descargas/sustento-descargas.component';
import { ComentarioDialogComponent } from './components/detalle-registro-pase/comentario-dialog/comentario-dialog.component';
//import { MglTimelineModule } from 'angular-mgl-timeline';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    HomeComponent,
    IndexComponent,
    ErrorComponent,
    LoadingComponent,
    UsuarioComponent,
    MsgErrorComponent,
    GestionRegistroPaseComponent,
    DialogConfirmComponent,
    MsgSucessComponent,
    DetalleRegistroPase,
    DatosGeneralesComponent,
    ArchivoComponent,
    ObservacionesComponent,
    ObjetosComponent,
    InformacionSoporteComponent,
    SustentoComponent,
    ComentarioComponent,
    FlujoReporteComponent,
    NuevoFormularioDialogComponent,
    SimpleSpacesDirective,
    SustentoDescargasComponent,
    ComentarioDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatTableModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatStepperModule,
    HttpClientModule,
    MatRadioModule,
    MatTooltipModule,
    MatSortModule,
    MatButtonModule
  ],
  // providers: [AuthGuard],
  // bootstrap: [AppComponent]
  providers: [AuthGuard,
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {dateInput: ['l','L']},
        display: {
          dateInput: 'L',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY'
        }
      }
    }
  ],
  bootstrap: [AppComponent],
  entryComponents:[
    DialogConfirmComponent
  ]
})
export class AppModule { }
