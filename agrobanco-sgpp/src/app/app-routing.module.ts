import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import {HomeComponent} from './components/home/home.component';
import { IndexComponent } from './components/redirect/index/index.component';
import { ErrorComponent } from './components/error/error.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { GestionRegistroPaseComponent } from './components/gestion-registro-pase/gestion-registro-pase.component';
import { DetalleRegistroPase } from './components/detalle-registro-pase/detalle-registro-pase.component';

const routes: Routes = [
  { path:'home', component: HomeComponent, canActivate: [AuthGuard]},
  { path:'usuario', component: UsuarioComponent, canActivate: [AuthGuard] },
  { path:'error', component: ErrorComponent },  
  { path:'Redirect/Index', component: IndexComponent },  
  { path:'gestion-pase-produccion', component: GestionRegistroPaseComponent  , canActivate: [AuthGuard] },
  { path:'crear-registro-pase/:idTipoPlataforma', component: DetalleRegistroPase, canActivate: [AuthGuard] },
  { path:'detalle-registro-pase/:idPase', component: DetalleRegistroPase, canActivate: [AuthGuard] },
  { path: '**' , pathMatch: 'full', redirectTo: 'home'}  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
