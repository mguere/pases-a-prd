
export enum TipoPlataforma{
    AplicacionWeb = 1,
    AS400_IBS = 2,
    PaginasWeb = 3,
    Spring = 4
}

export enum Estados{
    DerEjecutivo = 1,
    DerCalidad = 2,
    DerSoporte = 3,
    DevAnalista = 4
}

export enum Perfiles{
    ANA = "ANA",
    EJS = "EJS",
    CAL = "CAL",
    Soporte = "SOP",
}

export enum TipoObjeto{
    QDDSSRC = 1,
    QCLSRC = 2,
    QRPGSRC = 3
}

export enum TipoDerivacion{
    Aprobar = 1,
    Rechazar = 2,
    Anular = 3
}